<?php
include_once ("functions.inc");
// promote which site we are!
$site = "cs";

// use new style
$templatepath = "newlayout/";

// promote title to use
$site_title = i18n_var("České stránky o KDE");

$site_external = true;
$name = "Webmaster";
$mail = "jiri.vetvicka@blue-point.cz";
$showedit = True;

// links in the top
$menuright = array ('contact/index.php'=>'Kontakt');

#include( "site_includes/show_obtain_instructions.inc" );
#include( "site_includes/class_edugallery.inc" );
#include( "site_includes/edu_news.inc" );
#include( "site_includes/class_eduappinfo.inc" );

// some global variables
$kde_current_version = "3.5";
$kde_coming_version = "4";
$kde_stable_doc_path = "http://docs.kde.org/stable/en/kdeedu/";
$kde_development_doc_path = "http://docs.kde.org/development/en/kdeedu/";