<?php
  $page_title = "Mapa stránek";
  $site_root = "../";
  include "header.inc";
?>

<table>

<tr>
<td valign="top" style="width: 50%;"><h2>Informace</h2>
<ul>
<li><a href="http://www.kde.org/announcements/">Tiskové zprávy</a></li>
<li><a href="http://dot.kde.org">Novinky o KDE</a></li>
<li><a href="http://www.commit-digest.org">Víkendové statistiky KDE</a></li>
<!--<li><a href="http://web.archive.org/web/20070629173001/http://kdemyths.urbanlizard.com/">Myths about KDE</a></li>Nefunkcni link urbanlizard is gone, it seems -->
<li><a href="http://events.kde.org">Události</a></li>
<li><a href="http://enterprise.kde.org">KDE v podnikání</a></li>
<li><a href="http://ev.kde.org/">Association of K Desktop Environment</a></li>
<li><a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php">QT/KDE Free Foundation</a></li>
<li><a href="http://www.kde.org/history/">Historie KDE</a></li>
<li><a href="http://www.kde.org/awards/">Ocenění</a></li>
<li><a href="http://techbase.kde.org/ISV">Independent Software Vendors</a></li>
</ul></td>

<td valign="top"><h2>Centrum stahování</h2>
<ul>
<li><a href="http://www.kde.org/download/">Oficiální balíčky</a></li>
<li><a href="http://www.kde-apps.org">Aplikace</a> (neoficiální stránka)</li>
<li><a href="http://www.kde-look.org">Artwork</a> (neoficiální stránka)</li>
<li><a href="http://www.kde-files.org">Soubory a šablony</a> (neoficiální stránka)</li>
</ul>
<h3>Zdrojové kódy</h3>
<ul>
<li><a href="http://download.kde.org/download.php?url=stable/latest/src/">Oficiální verze</a> (<a href="http://www.kde.org/download/">Informace</a>)</li>
<li><a href="http://download.kde.org/download.php?url=snapshots/">Vývojové verze</a> (<a href="http://quality.kde.org/develop/cvsguide/buildstep.php">Informace</a>)</li>
</ul>
</td>
</tr>

<tr>
<td valign="top"><h2>Stránky pro uživatele</h2>
<ul>
<li><a href="http://forum.kde.org/" rel="nofollow">KDE Fórum</a></li>
<li><a href="http://docs.kde.org">Dokumentace</a></li>
<li><a href="http://www.kdedevelopers.org/">Blogy vývojářů</a> (neoficiální stránka)</li>
<li><a href="http://wiki.kde.org">Komunitní Wiki</a></li>
<li><a href="http://planetkde.org">Blogy od KDE lidí</a> (neoficiální stránka)</li>
<li><a href="http://kde.openoffice.org">Integrace OpenOffice.org v KDE</a></li>
<li><a href="/international/">Stránky KDE v jiných jazycích</a></li>
</ul></td>

<td valign="top"><h2>Nejčastěji používaný software</h2>
<ul>
<li><a href="http://konqueror.kde.org">Webový prohlížeč</a></li>
<li><a href="http://kmail.kde.org/">E-mailový klient</a></li>
<li><a href="http://kontact.org/">PIM</a></li>
<li><a href="http://kopete.kde.org">Instant Messaging (Messenger, ICQ, Jabber...)</a></li>
<li><a href="http://amarok.kde.org">Přehrávač muziky</a> (neoficiální stránka)</li>
<li><a href="http://kaffeine.sf.net">Přehrávač videa</a> (neoficiální stránka)</li>
<li><a href="http://koffice.org">Kancelářské řešení</a></li>
<li><a href="http://digikam.org">Správce digitálních fotoaparátů</a></li>
<li><a href="http://kpdf.kde.org">Prohlížeč PDF souborů</a></li>
<li><a href="http://edu.kde.org">Vzdělávací software</a></li>
<li><a href="http://www.k3b.org">Vypalování CD &#038; DVD</a></li>
</ul>
</td>
</tr>

<tr>

<td valign="top"><h2>Další software</h2>
<ul>
<li><a href="http://kdewebdev.org">Vývoj webových stránek</a></li>
<li><a href="http://www.kate-editor.org/">Textový editor</a></li>
<li><a href="http://konsole.kde.org">Emulátor konzole</a></li>
<li><a href="http://printing.kde.org">Tisk</a></li>
<li><a href="http://kooka.kde.org">Správce skenerů</a></li>
<li><a href="http://games.kde.org">Herní centrum</a></li>
<li><a href="http://noatun.kde.org">Přehrávač Noatun</a></li>
<li><a href="http://phonon.kde.org">KDE Sound System</a> (pro budoucí použití)</li>
<li><a href="http://multimedia.kde.org">Multimedia Center</a></li>
<li><a href="http://extragear.kde.org">Aplikace, které nejsou součástí oficiálního KDE vydání</a></li>
<li><a href="http://www.kde-apps.org">Databáze všech KDE aplikací</a> (neoficiální stránka)</li>
</ul>
</td>

<td valign="top"><h2>Software pro vývojáře</h2>
<ul>
<li><a href="http://kdesvn-build.kde.org">Build KDE ze SVN</a></li>
<li><a href="http://kdevelop.org">KDevelop</a></li>
<li><a href="http://kbabel.kde.org">KBabel</a></li>
</ul></td>

</tr>

<tr>
<td valign="top"><h2>Stránky pro vývojáře</h2>
<ul>
<li><a href="http://techbase.kde.org">Programmers Team Home</a></li>
<li><a href="http://lxr.kde.org">Source Cross reference</a></li>
<li><a href="http://websvn.kde.org">WebSVN</a></li>
<li><a href="http://bugs.kde.org">Databáze chyb</a></li>
<li><a href="http://lists.kde.org">Mailingové listy</a></li>
<li><a href="http://techbase.kde.org/ISV">Independent Software Vendors</a></li>
<li><a href="http://qtcentre.org">Qt Users Community site</a></li>
</ul></td>

<td valign="top"><h2>Vývojářské týmy</h2>
<ul>
<li><a href="http://techbase.kde.org">Programátoři</a></li>
<li><a href="http://quality.kde.org">Záruka kvality</a></li>
<li><a href="http://usability.kde.org">Použitelnost</a></li>
<li><a href="http://accessibility.kde.org">Přístupnost</a></li>
<!-- <li><a href="http://kde-artists.org">Artists</a></li> Nefungující odkaz-->
<li><a href="http://l10n.kde.org">Překladatelé</a></li>
<li><a href="http://l10n.kde.org/docs/">Documentace</a> (nedokončeno)</li>
<li><a href="http://freebsd.kde.org">FreeBSD Packagers</a></li>
<li><a href="http://solaris.kde.org">Solaris Packagers</a></li>
<li><a href="http://women.kde.org">Ženy v KDE</a></li>
<!-- <li><a href="http://appeal.kde.org">Appeal</a></li> Jen presmerovani na kde.org-->
<li><a href="http://spread.kde.org">Propagace</a></li>
</ul></td>

</tr>

</table>

<?php
 include "footer.inc";
?>
