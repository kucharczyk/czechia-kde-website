<?php
  $page_title = "KDE v jiných jazycích";
  include "header.inc";
?>
<style type="text/css" media="screen">
LI {list-style-type:none;}
</style>
<p>Tato stránka nabízí přístup ke KDE webům i v ostatních jazycích. Cílem je poskytnout informace pro danou komunitu lidí v jejich rodném jazyce. Zároveň tyto stránky nemusí být zapojeny do překladu KDE o němž se více dozvíte na <b><a href="http://l10n.kde.org">l10n.kde.org</a></b>.
</p>
<p>
Pokud se chcete zapojit do překladu českých stránek, kontaktujte <a href="mailto:jiri.vetvicka@blue-point.cz">webmastera</a>.
</p>

<!--
IMPORTANT: Please remember to also change www/media/includes/header.inc if you add or remove sites here!
//-->


<table summary="This table contains all KDE user group websites around the world">
<tr>
<td valign="top" style="padding:1em;">
<h2 style="text-align:center;">Evropa &#038; Střední východ</h2>
<ul>
<li><img src="../img/flags/ba.png" width="22" height="14" alt="[Bosnia Flag]" /> <a href="http://kde.linux.org.ba/">Bosna</a>&#160;(neaktualizováno)</li>
<li><img src="../img/flags/bg.png" width="22" height="14" alt="[Bulgaria Flag]" /> <a href="http://kde.tilix.org/">Bulharsko</a></li>
<li><img src="../img/flags/cz.png" width="22" height="14" alt="[Czech Republic Flag]" /> <a href="http://czechia.kde.org/">Česká republika</a></li>
<li><img src="../img/flags/fr.png" width="22" height="14" alt="[France Flag]" /> <a href="http://fr.kde.org/">Francie</a></li>
<li><img src="../img/flags/de.png" width="22" height="14" alt="[Germany Flag]" /> <a href="http://de.kde.org/">Německo</a></li>
<li><img src="../img/flags/gb.png" width="22" height="14" alt="[Great Britain Flag]" /> <a href="http://www.kde.org.uk/">Velká Británie</a></li>
<!-- very old <li><img src="../img/flags/hu.png" width="22" height="14"alt="Hungary" /> <a href="http://www.kde.hu/">Hungary</a></li> -->
<li><img src="../img/flags/is.png" width="22" height="14" alt="[Iceland Flag]" /> <a href="http://www.is.kde.org/">Island</a></li>
<li><img src="../img/flags/ie.png" width="22" height="14" alt="[Ireland Flag]" /> <a href="http://www.kde.ie/">Irsko</a></li>
<li><img src="../img/flags/il.png" width="22" height="14" alt="[Israel Flag]" /> <a href="http://il.kde.org/">Izrael</a></li>
<li><img src="../img/flags/it.png" width="22" height="14" alt="[Italy Flag]" /> <a href="http://www.kde-it.org/">Itálie</a></li>
<li><img src="../img/flags/nl.png" width="22" height="14" alt="[Netherlands Flag]" /> <a href="http://www.kde.nl/">Holandsko</a></li>
<li><img src="../img/flags/pl.png" width="22" height="14" alt="[Poland Flag]" /> <a href="http://www.kde.org.pl/">Polsko</a></li>
<!-- Single page? <li><img src="../img/flags/pl.png" width="22" height="14"alt="[Poland Flag]" /> <a href="http://kde.com.pl/">Poland (old)</a></li> -->
<li><img src="../img/flags/ro.png" width="22" height="14" alt="[Romania Flag]" /> <a href="http://www.ro.kde.org/">Rumunsko</a></li>
<li><img src="../img/flags/ru.png" width="22" height="14" alt="[Russia Flag]" /> <a href="http://www.kde.ru/">Rusko</a></li>
<!-- Non existent directory <li><a href="/international/slovakia/"><img src=../img/flags/sk.png" width="22" height="14"alt="Slovakia" />Slovakia</a></li> -->
<li><img src="../img/flags/es.png" width="22" height="14" alt="[Spain Flag]" /> <a href="http://es.kde.org/">Španělsko</a></li>
<li><img src="../img/flags/tr.png" width="22" height="14" alt="[Turkey Flag]" /> <a href="http://www.kde.org.tr/">Turecko</a></li>
<li><img src="../img/flags/ua.png" width="22" height="14" alt="[Ukraine Flag]" /> <a href="http://www.kde.org.ua/">Ukrajina</a></li>
<li><img src="../img/flags/yu.png" width="22" height="14" alt="[Yugoslavia Flag]" /> <a href="http://www.kde.org.yu/">Jugoslávie</a>&#160;(neaktualizováno)</li>

</ul>
</td>
<td valign="top" style="padding:1em;">
<h2 style="text-align:center;">Asie</h2>
<ul>
<li><img src="../img/flags/cn.png" width="22" height="14" alt="[China Flag]" /> <a href="http://www.kdecn.org/">Čína</a></li>
<li><img src="../img/flags/in.png" width="22" height="14" alt="[India Flag]" /> <a href="http://in.kde.org/">Indie</a></li>
<li><img src="../img/flags/jp.png" width="22" height="14" alt="[Japan Flag]" /> <a href="http://www.kde.gr.jp/">Japonsko</a></li>
<!--<li><img src="/img/flags/kr.png" width="22" height="14"alt="[Korea Flag]" /> <a href="http://www.kde.or.kr/">Korea</a></li>-->
<li><img src="../img/flags/tw.png" width="22" height="14" alt="[Taiwan Flag]" /> <a href="http://kde.linux.org.tw/" rel="nofollow">Taiwan</a></li>
</ul></td>

<td valign="top" style="padding:1em;">
<h2 style="text-align:center;">Amerika</h2>
<ul>
<li><img src="../img/flags/br.png" width="22" height="14" alt="[Brasil Flag]" /> <a href="http://br.kde.org/">Brazílie</a></li>
<li><img src="../img/flags/cl.png" width="22" height="14" alt="[Chile Flag]" /> <a href="http://www.kde.cl/">Čile</a></li>
</ul>
</td>
</tr>
</table>



<p style="font-size: smaller;">
<b>Note:</b><br />

Our policy regarding the sites listed above is simple: If somebody goes through the
trouble of creating a KDE website, then we will include it in this
list.  Furthermore, we will <em>not</em> override the site creator's
preference for what to call their geographical area (e.g., calling the
Taiwan site "Taiwan" instead of "China").
</p>

<?php
  include "footer.inc";
?>
