<?php
  $site_onemenu = true;
  $site_root = "../";
  $site_title = "Vychází KDE 2.2";
  include_once ("$site_root/media/includes/header.inc");
?>

<h2 align="center">Nová verze KDE připravena pro podnikovou sféru</h2>
<p>
15. srpna 2001 (Internet) -
<a class="cs" href="/">Tým KDE</a>
dnes oznámil vydání KDE 2.2, mocného a snadno
použitelného rozhraní s podporou Internetu pro
Linux a další Unixy. Díky rychlému a
ukázněnému vývoji obsahuje KDE 2.2 celou řadu
zdokonalení týkajících se rychlosti a
stability, stejně jako velké množství
nových vylepšení. KDE 2.2 je k dispozici ve
<a class="en" href="http://i18n.kde.org/teams/distributed.html">34</a>
jazycích. Skládá se ze základních knihoven
KDE, vlastního grafického rozhraní a více než
100 aplikací z dalších standardních balíčků
(administrace, multimédia, síť, PIM, vývoj,
atd.).
</p><p>
"Pracovní nástroje a vylepšené rozhraní nového
KDE nabízejí mocné prostředí pro pracovní
stanice," oznámil Kent Freson, vice-president
divize
<a class="en" href="http://www.tru64unix.compaq.com/index.html">Tru64 UNIX Systems</a>
společnosti <a class="en" href="http://www.compaq.com/index.html">Compaq</a>.
"Jsme potěšeni, že můžeme KDE 2.2 nabídnout
pro systém Tru64 UNIX a zlepšit tak
univerzálnost a jednodušší přenositelnost
aplikací. Compaq vyjádřil uznání Dr. Tomu
Leitnerovi z Technické univerzity v Grazu a
vývojovému týmu KDE za jejich podíl na
vylepšení těchto vlastností."
</p><p>
 Prostředí KDE 2.2 bude ještě tento měsíc
doplněno o stabilní verzi
<a class="cs" href="http://www.koffice.org">KOffice 1.1</a>,
souhrnný modulární balík kancelářských
aplikací. Tato kombinace nabízí jako první
kompletní OpenSource pracovní prostředí pro
Linux/Unix.
</p><p>
KDE a všechny jeho komponenty jsou k dispozici
zdarma pod OpenSource licencí na
<a class="en" href="ftp://ftp.kde.org/pub/kde/stable/2.2/">serveru</a>
KDE
(<a class="en" href="/mirrors/ftp.php">zrcadel</a>včetně zrcadel)
a nebo mohou být získány na
<a class="cs" href="/download/cdrom.php">CD-Rom</a>.
</p><p>
 "Díky velmi kvalitní vývojové základně KDE 2 a
neocenitelné pomoci uživatelů nabízíme
vylepšené, lépe integrované pracovní prostředí
s novými rysy v relativně krátké době. Ještě
tento měsíc vyjde stabilní verze KOffice a KDE
tak nabídne kompletní, vysoce kvalitní
pracovní a vývojové prostředí nezatížené
náklady a omezeními proprietárních prostředí,"
řekl Waldo Bastian, release-manager KDE 2.2 a
vývojář SuSE Labs.
</p><p>
Dave Richards, systémový administrátor radnice
města
<a class="en" href="http://www.largo.com/index.html">Largo</a>
(stát USA - Florida), kde bylo v nedávné době
úspěšně nasazeno 400 ultralehkých klientských
stanic pro 800 uživatelů, vysvětluje, proč
používají KDE a Linux: "Všechny terminály
radnice jsou propojeny do jednoho velkého
'desktopového' systému s KDE. Protože
stabilita tohoto serveru je naší nejvyšší
prioritou, byl vybrán Linux. KDE se jeví jako
vynikající řešení prezentační vrstvy, na které
běží všechny aplikace spouštěné na našich
serverech (Unix i Windows)."
</p><p>
 "Je důležité, aby si podnikatelé uvědomili, že
velké úspory na nákladech vzniklé přechodem na
KDE nejsou podmíněny zhoršením aplikačních
schopností pro uživatele," prohlásil
Andreas Pour, předseda Ligy KDE. "Schopnosti
prohlížeče Konqueror, neustále se rozšiřující
množství volně dostupného softwaru pro KDE/Qt
a velký počet dalších OpenSource projektů
vyřeší většinu, ne-li všechny problémy s
dostupností aplikací. Pro aplikace dostupné
pouze pro Windows existuje několik
alternativních řešení (ať již komerčních nebo
OpenSource): spouštění sezení Windows
terminálů v KDE, spouštění Windows aplikací
přímo pod KDE/Linux nebo vzdálené sezení.
Neexistuje důvod, proč by společnosti nemohly
ušetřit alespoň část nákladů vydávaných na
licence klientských stanic.
</p><p>
<a class="cs" href="http://www.konqueror.org">Konqueror</a>.
je webový prohlížeč, souborový
manažer a prohlížeč dokumentů nové generace.
Modulární architektura Konqueroru kombinuje
schopnosti a funkce prohlížečů Internet
Explorer/Netscape Communicator a Průzkumníka
Windows. Konqueror podporuje běžné Internetové
technologie, jako JavaScript, Java, XML, HTML
4.0, CSS-1 a 2 (kaskádovité styly), SSL
(Secure Socket Layer pro šifrovanou
komunikaci) a plug-iny prohlížeče Netscape
Communicator (pro Flash, RealAudio, RealVideo
a podobné technologie). Dále jsou podporovány
některé ActiveX prvky, např. přehrávač
ShockWave. Podpora pro všechny tyto standardy
je poskytována prostřednictvím KHTML, součásti
knihovny KDE, která je dostupná pro všechny
aplikace KDE jako objekt (prostřednictvím
běžného dědění oken) nebo jako komponenta
(prostřednictvím technologie KParts).
</p><p>
KDE nabízí plně transparentní síťový přístup k
souborům v Linuxu, prostřednictvím NFS
sdílení, MS Windows sdílení (SMB protokol),
HTTP stránek, FTP adresářů, LDAP adresářů,
digitálních kamer a audio CD. Modulární
architektura přístupu k souborům umožňuje
jednoduchým způsobem používat další protokoly
(jako např. IPX nebo WebDAV) a zpřístupnit
takto získané soubory všem KDE aplikacím.
</p><p>
Kromě výjimečné kompatibility s internetovými
standardy a standardy pro sdílení souborů, je
KDE 2 vůdčím prostředím také v oblasti
dodržování standardů pracovní plochy Linuxu.
Nově přepracovaný správce oken KWin splňuje
nejnovější specifikace pro správce oken.
Konqueror a KDE používají standardní položky
pracovní plochy. Pro systém záložek používá
Konqueror standard XBEL. KDE 2 je plně
kompatibilní s protokolem X Drag-and-Drop
(XDND), stejně tak i s protokolem X11R6
session management (XSMP).
</p><p>
KDE 2.2 obsahuje velké množství vylepšení,
která odstraňují překážky bránící většímu
rozšíření KDE v podnikové sféře:
</p>
<ul>
<li> až o 50 % rychlejší spouštění aplikací v
systému GNU/Linux prostřednictvím
experimentální metody před-linkování
objektových souborů;</li>
<li> zlepšená stabilita a schopnosti vykreslování
HTML a JavaScriptu;</li>
<li> podpora protokolu IMAP (včetně SSL a TLS) v
poštovním programu KMail;</li>
<li> nová modulární architektura tiskového
systému s integrovanými filtry a schopnostmi
pro úpravu vzhledu stránky (v současné době
podporuje LPR, CUPS, RLPR, externí příkazy,
nativní LPD a pseudo-tisková zařízení pro
odesílání faxů nebo e-mailů);</li>
<li> rychlejší start systému a odezva;</li>
<li> několik nových pluginů pro Konqueror, např.
překladač BabelFish, generátor galerií
obrázků, systém pro kontrolu HTML a archivátor
webů;</li>
<li> nativní podpora iCalendaru v programu
KOrganizer;</li>
<li> nový průvodce osobním nastavením.</li>
</ul>
<p>
KDE 2 nabízí sofistikované vývojové prostředí
a velký počet technologických vylepšení oproti
kritizované sérii KDE 1. Nejdůležitějšími technologiemi
jsou Desktop COmmunication Protocol (DCOP),
I/O knihovny (KIO) a objektový model komponent
(KParts), třída GUI založená na XML a systém
pro vykreslování HTML, kompatibilní s
Internetovými standardy (KHTML).
</p><p>
KDevelop je vůdčí linuxové integrované
vývojové prostředí s velkým počtem funkcí pro
rychlý vývoj aplikací (RAD) obsahující návrhář
GUI dialogových oken, integrovaný debugger,
správce projektů, funkce pro tvorbu
dokumentace a překlady, zabudovanou podporu
soustředěného vývoje a mnoho dalšího. Tato
verze obsahuje další nové vlastnosti včetně
průvodce nastavením, navigaci zdrojovým kódem,
konzoli, podporu manuálových stránek, barevně
odlišenou syntax, stejně tak i celou řadu
nových šablon projektů jako např. moduly
Ovládacího centra, aplety Kickeru (panelu),
KIO-slaves, pluginy pro Konqueror nebo témata
pracovní plochy.
</p><p>
Osvědčený objektový model komponent KParts
obsluhuje všechny vsazené části aplikací,
např. nástrojové lišty nebo kontextově senzitivních
nabídky. KParts také
spolupracuje s KIO při práci s MIME typy nebo
službami/protokoly. Tato technologie je hojně
použita v KOffice a Konqueroru.
</p><p>
KIO implementuje vstupy a výstupy aplikací v
odděleném procesu. Díky tomu není třeba při
vytváření GUI používat vlákna. Práce s různými
protokoly je naprosto transparentní, a proto
může být tato třída použita pro přístup k
datům pomocí HTTP, FTP, POP, IMAP, NFS, SMB,
LDAP nebo lokálního souborového systému.
Pomocí modulárně rozšiřitelného návrhu
umožňuje vývojářům přidávat podporu dalších
protokolů, které budou dostupné všem KDE
aplikacím. KIO je schopné obsluhovat různé
MIME typy a prostřednictvím KParts je vkládat
do aplikací.
</p><p>
DCOP je client-to-client komunikační protokol
zprostředkovaný serverem pomocí standardní X11
ICE knihovny. Protokol podporuje zpracování
zpráv a volání vzdálených procedur pomocí
brány mezi XML-RPC a DCOP. K dispozici jsou
propojení s C, C++ a Python, experimentálně
také Java.
</p><p>
KDE dále podporuje propojení programovacích
jazyků. Kdejava nabízí propojení Javy s
KDE/Qt, které se chová a vypadá stejně jako
C++ verze, včetně přístupu k signálům a slotům
napsaným v C++.
</p>

<b>Instalace binárních balíčků</b><br />
<p>
Binární balíčky. Všichni hlavní distributoři
Linuxu a někteří distributoři Unixu poskytují
binární balíčky KDE 2.2 pro poslední verze
svých distribucí. Některé z těchto balíčků
jsou k dispozici pro stažení na
<a class="en" href="http://ftp.sourceforge.net/pub/mirrors/kde/stable/2.2/">http://ftp.sourceforge.net/pub/mirrors/kde/stable/2.2/</a>
nebo pod ekvivalentním adresářem na
zrcadlových FTP
<a class="en" href="/mirrors/ftp.php">serverech</a>.
Je třeba upozornit, že tým KDE
neodpovídá za tyto balíčky, protože jsou
poskytovány třetí stranou - obvykle (ne však
vždy) výrobcem dané distribuce. (V případě, že
nemůžete najít binární balíčky pro vaší
distribuci, přečtěte si prosím dokument KDE
<a class="en" href="http://dot.kde.org/986933826/">Binary Package Policy</a>).
</p><p>
Požadavky na knihovny. Požadavky na knihovny
závisí na systému, na kterém byl balíček
vytvořen. Některé
binární balíčky vyžadují novější verzi Qt a
dalších knihoven, než jsou v distribuci (např.
LinuxDistro 8.0 obsahuje qt-2.2.3, ale binární
balíčky s KDE vyžadují qt-2.3.x). Další
informace o knihovnách jsou v části Zdrojové
kódy - požadavky na knihovny.
</p><p>

Umístění balíčků. V okamžiku vydání KDE 2.2
jsou k dispozici <a class="cs" href="/download/" >binární balíčky</a>
pro
následující distribuce:
</p>
<ul>
<li>Caldera System</li>
<li> OpenLinux 3.1</li>
<li> Conectiva Linux 7.0</li>
<li> Debian GNU/Linux</li>
<li> FreeBSD</li>
<li> IBM AIX</li>
<li> Linux Mandrake 7.2 a 8.0 </li>
<li> RedHat Linux 7.x</li>
<li> SuSE Linux 6.4, 7.0, 7.1, 7.2</li>
<li> Tru64 systems</li>
</ul>

<b>Stažení a kompilace KDE 2.2</b><br />
<p>
Požadavky na knihovny. KDE 2.2 vyžaduje
qt-2.2.4, které je dostupné ve formě
zdrojových kódů u společnosti Trolltech jako
qt-x11-2.2.4.tar.gz. Pro podporu SSL vyžaduje
KDE 2.2 knihovnu OpenSSL >= 0.9.6x, verze
0.9.5x nejsou podporovány. Pro podporu Javy
vyžaduje KDE 2.2 knihovnu JVM >= 1.3. Pro
podporu pluginů programu Netscape je zapotřebí
poslední verze Lesstif a Motif. Vyhledávání
lokálních dokumentů vyžaduje htdig. Ostatní
speciální vlastnosti, např. drag-n-drop
grabování audio CD, mohou vyžadovat další
knihovny.
</p><p>
Požadavky na kompilátor. Mějte na paměti, že
některé komponenty KDE 2.2 nelze zkompilovat
se staršími verzemi gcc/egcs, jako např.
egcs-1.1.2 nebo gcc-2.7.2. Za minimum je
považováno gcc-2.95-*. Některé komponenty KDE
2.2 nelze zkompilovat pomocí gcc 3.0 (na
odstranění problémů s touto verzí překladače
intenzivně pracují týmy KDE a GCC).
</p><p>
Zdrojové kódy/RPM balíčky. Kompletní zdrojové
kódy KDE 2.2 jsou k dispozici volně ke stažení
na
http://master.kde.org/pub/kde/stable/2.2/src
nebo pod ekvivalentním adresářem na
zrcadlových FTP serverech. Zdrojové RPM
balíčky jsou k dispozici pro následující
distribuce:
</p>
<ul>
<li> Conectiva Linux</li>
<li> Caldera Systems</li>
<li> Linux Mandrake</li>
<li> RedHat Linux</li>
<li> SuSE Linux</li>
</ul>
<p>
Další informace. Další informace o kompilaci a
instalaci KDE 2.2 naleznete v dokumentu
Installation instructions. V případě, že se
vyskytnou problémy, nahlédněte do Compilation
FAQ. Při problémech s RPM balíčky kontaktujte
osobu uvedenou v souboru .spec.
</p>

<?php include_once("$site_root/media/includes/footer.inc"); ?>
