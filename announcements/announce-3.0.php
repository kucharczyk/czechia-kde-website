<?php
  $site_onemenu = true;
  $site_root = "../";
  $site_title = "Vychází KDE 3.0";
  include_once ("$site_root/media/includes/header.inc");
?>

<p>3. duben, 2002</p>

<h3 align="center">
   Projekt KDE uvolňuje novou verzi vedoucího Linuxového desktopového prostředí
   a vítá podnikovou sféru do světa svobodného softwaru
</h3>

<p>
  <strong>
    Projekt KDE uvolňuje třetí generaci vedoucího desktopového prostředí
    pro Linux/UNIX,
    a nabízí tak podnikům, vládám i školám skvělé a otevřené řešení pro 
    pracovní stanice a domácí počítače.
  </strong>
</p>

<p>
  3. duben, 2002 (INTERNET).  <a href="/">Tým KDE
  </a> dnes oznámil vydání KDE 3.0,
  třetí generace <em>otevřeného</em> prostředí KDE, mocného grafického prostředí
  pro Linux a další UNIXy.  KDE 3.0 je dostupné v 49 jazycích a je dodáváno
  se základními knihovnami KDE, vlastním grafickým prostředím,
  <a href="http://www.koffice.org">kancelářským balíkem</a>
  <a href="http://www.kdevelop.org/">integrovaným vývojovým prostředím</a> a
  stovkami aplikací a dalších rozšíření z ostatních základních balíků KDE
  (PIM, administrace, síť, výuka, vývoj, pomůcky,
  multimédia, hry a další). V souladu s rychlým a disciplinovaných vývojem KDE
  je pro KDE 3.0 k dispozici obsáhlý seznam hlavních výkonových vylepšení a nových vlastností.
</p>
<p>
  KDE, včetně všech KDE knihoven a aplikací, je
  dostupné <em><strong>zdarma</strong></em> licencované pod Open Source licencemi.
  KDE může být získané ve zdrojových souborech nebo některém z mnoha binárních formátů
  z <a href="http://download.kde.org/stable/3.0/">http serverů</a>
  nebo <a href="/mirrors/ftp.php">ftp zrcadel</a> KDE,
  na <a href="/download/cdrom.php">CD-ROM</a> nebo s kterýmkoliv
  dnešním hlavním Linux/UNIX systémem.
</p>
<p>
  "Více a více dílků se spojuje pro vytvoření životaschopného Linuxového
  desktopu," řekl Bernd Kosch, Viceprezident marketingové strategií a spojení
  z firmy <a href="http://www.fujitsu-siemens.com/linux/">Fujitsu
  Siemens Computers</a>. "KDE 3 je důležitý krok kupředu směrem k tomuto cíli,
  protože ISV budou mít usnadněné portování na platformu Linux/KDE.
  Sledujeme zvětšující se zájem o Linux a KDE a oceňujeme 
  výborné výsledky vývojového týmu KDE."
</p>
<p>
  "Jako odpověď na požadavky zákazníků se KDE stalo implicitním grafickým prostředím
  v našem produktu Turbolinux Workstation," řekl Dino Brusco,
  Marketing VP, <a href="http://www.turbolinux.com/">Turbolinux Inc.</a>
  "Naši zákazníci skutečně oceňují vlastnosti a stabilitu, kterou KDE nabízí,
  a budeme poskytovat tuto nejnovější verzi KDE v budoucích verzích našeho produktu
  Turbolinux Workstation."
</p>
<p>
  "Design, výkon a zlepšení produktivity, které KDE 3.0 poskytuje pro Linux
  na desktopu, na nás udělaly velký dojem," řekl Carsten
  Fischer, <a href="http://www.suse.cz/">SuSE Linux</a> Product Manager.
  "KDE 3 skvěle zapadá do naší strategie poskytovat našim uživatelům
  jednoduché ale výkonné nástroje pro provádění jejich denních činností
  na Linuxu, a jsme velmi potěšeni, že KDE 3.0 bude součástí naší nové
  verze 8.0 vydané později tento měsíc."
</p>
<p>
  "KDE - kombinované se systémem GNU/Linux nebo UNIX - nabízí zajímavé řešení
  pro podniky, které chtějí realizovat značné úspory v oblasti IT,
  a přichází ve vhodný čas ve světle současné ekonomické situace
  a zvětšujících se licenčních poplatků," dodal
  Andreas Pour, předseda <a href="http://www.kdeleague.org/">Ligy KDE</a>.
  "KDE umožňuje podnikům, vládám, školám, malým firmám, charitám a 
  ostatním institucím zbavit se podstatné části
  šílených licenčních poplatků, omezení a kontrol.  V podstatě myslím, že by se
  dalo říct, že KDE otevírá brány svobody pro podnikovou sféru."
</p>
<p>
  Více informací o využití KDE v podnikovém prostředí lze
  <a href="http://www.kdeleague.org/kgx.php">najít</a>
  na <a href="http://www.kdeleague.org/">stránkách</a> Ligy KDE.
</p>
<p>
  KDE 2 aplikace budou pracovat v prostředí KDE 3, pokud jsou nainstalované
  KDE 2 knihovny.  Nicméně, mnoho KDE 2 aplikací je již portováno pro KDE 3,
  vzhledem k tomu, že je to <a href="#porting">relativně
  jednoduché</a> a výhody jsou značné.
</p>

<h4>
  K Desktop Environment 3.0 (grafické prostředí KDE 3.0)
</h4>
<p>
  <u>Tisk</u>.
  <a name="kdeprint"></a>
  Jednou z nejzajímavějších věcí v KDE 3 je nový tiskový systém
  <a href="http://printing.kde.org/">KDEPrint</a>.
  Jeho modulární design umožňuje jednoduše podporovat různé tiskové
  systémy, jako CUPS, LPRng, LPR, LPD nebo další servery nebo programy.
</p>
<p>
  KDEPrint se skládá z dialogu pro tisk, tiskového manažera,
  prohlížeče tiskových úloh pro řízení tiskové fronty, editoru
  pro vytváření sérií externích filtrů tiskových úloh jako
  <a href="http://people.ssh.fi/mtr/genscript/">enscript</a>,
  <a href="http://www-inf.enst.fr/~demaille/a2ps/">a2ps</a> a
  pamflet a poskytující grafickou konfiguraci těchto filtrů,
  tiskového průvodce pro automatickou detekci a instalování nových tiskáren,
  a konfiguračního nástroje pro CUPS.
</p>
<p>
  Ve spojení s CUPS, KDEPrint nyní dokáže řídit promyšlený
  podnikový síťový tiskový systém. Nové vlastnosti zahrnují
  kompletní administrativní kontrolu více tiskových front a
  tiskových úloh, tisk pomocí drag&amp;drop, plánování tiskových úloh,
  priority tiskových úloh, podpora pro účtování a plánování,
  tiskové fronty s možností migrace tiskových úloh,
  tisková omezení každé tiskárny, možnost autorizace uživatelů pro tisk
  a omezení seznamu tiskáren, které jsou vypsány v dialogu výběru tiskárny
  individuálních uživatelů.
</p>
<p>
  Protože KDEPrint poskytuje přístup z příkazové řádky, je přístupný, včetně
  grafických konfiguračních dialogů, z jakékoliv aplikace (nejen KDE),
  která umožňuje uživateli nastavit si tiskový příkaz, např. StarOffice,
  OpenOffice, WordPerfect 2000, Netscape, Mozilla, Galeon, Acrobat Reader, gv
  a mnoho dalších.
</p>
<p><u>Jazyková podpora</u>.
  KDE 3.0 přichází v 49 jazycích včetně češtiny a další jazyky jsou
  očekávány pro verzi KDE 3.0.1. Jakákoliv komunita může přeložit
  KDE do svého jazyka pomocí pokročilých překladatelských nástrojů KDE
  bez jakýchkoliv znalostí programování.
</p>
<p>
  Působivá podpora jazyků v KDE je možná hlavně díky používání Unicode
  (včetně Unicode 3). KDE také podporuje jazyky píšící zprava doleva
  jako hebrejština nebo arabština. Více znakových sad a směrů
  může být použito ve stejném dokumentu, dokonce i bez použití Unicode
  fontů.
</p>
<p>
  <u>Prohlížeč</u>.
  <a name="Konqueror"></a><a href="http://www.konqueror.org">Konqueror</a>
  je KDE WWW prohlížeč páté generace, správce souborů a prohlížeč dokumentů.
  Standardy podporující Konqueror má komponentově založenou architekturu,
  která kombinuje vlastnosti a funkcionalitu Internet
  Exploreru/Netscape Communicatoru a Windows Exploreru.
</p>
<p>
  <a name="konq-internet">Konqueror používá KHTML</a> jako renderovací jádro.
  KHTML podporuje plnou škálu současných internetových technologií,
  podporuje jazyky JavaScript a Java, XML 1.0 a HTML 4.0,
  kaskádové styly (CSS-1 a -2), bezpečné spojení pomocí SSL,
  pluginy pro Netscape Communicator zahrnující
  <a href="http://macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash">Flash</a>,
  <a href="http://www.real.com/player/index.html?src=010709realhome_1">RealAudio
  a RealVideo</a>, ve spojení s některými přídavnými
  <a href="http://www.codeweavers.com/products/crossover/">komerčními
  rozšířeními</a>, Windows Netscape pluginy, včetně přehrávačů
  <a href="http://macromedia.com/software/shockwaveplayer/">Macromedia
  Shockwave Player</a>, 
  <a href="http://www.apple.com/quicktime/">QuickTime</a> a Windows
  Media Player 6.5, a různými prohlížeči dokumentů MS Office.
</p>
<p>
  "Jedním z největších vylepšeních KDE 3.0 oproti KDE 2.2 je podpora
  JavaScriptu/DHTML v Konqueroru," řekl
  <a href="http://www.kde.org/people/dirk.html">Dirk Mueller</a>,
  vývojář KHTML a koordinátor verze KDE 3.0 .
  "Model DOM 2, používaný pro renderování HTML stránky, je teď plně implementován,
  a změny v DOM stromu jsou nyní zpracovávány mnohem lépe.
  Podpora JavaScriptu je téměř kompletní, rychlejší a stabilnější
  než v KDE 2, a dokonce jsme přidali možnost "smart" ošetřování vyskakovacích oken,
  takže řízení oken je nyní zpět pod kontrolou uživatele, kde by mělo zůstat.
  Tato výsledná zlepšení v rychlosti a kvalitě renderování dynamických webových
  stránek je něco, co uživatelé okamžitě ocení."
</p>
<p>
  Vylepšená podpora JavaScriptu muže být příhodně demonstrována
  <a href="http://www.webreference.com/dhtml/column65/">kompatibilitou
  </a> s Hiermenus, sofistikovaným JavaScriptovým systémem pro 
  pop-up a drop-down menu. Tato verze také přidává kompletní nástroje
  pro správu SSL certifikátů a certifikačních autorit, a také správu animovaných GIF
  obrázků.
</p>
<p>
  <u>Správce souborů</u>.
  Konqueror je taktéž správcem souborů pro KDE. V KDE 3.0 Konqueror
  dokáže navíc zobrazit další informace o souborech - velikost souboru, práva
  a data specifická pro různé typy souborů jako např. ID3 tagy pro MP3
  soubory nebo komentáře pro JPG obrázky - v tooltipech, a ve vhodných
  případech umožňuje také tyto informace měnit.
  Dále Konqueror poskytuje audio "náhledy" při umístění myši nad ikonou
  souboru. Postranní lišta v Konqueroru nyní také obsahuje přehrávač,
  do kterého lze pomocí Drag&amp;Drop přetahovat audio/video soubory
  pro rychlé přehrávání.
</p>
<p>
  <u>Email</u>.
   <a href="http://kmail.kde.org/">KMail</a> je plně vybaveným uživatelsky
   přívětivým mailovým klientem pro KDE. KMail podporuje oba populární mailové
   standardy IMAP a POP3. Uživatelé mohou mít více účtů a více identit.
   Adresář je založen na standardu vCard a je sdílen je zbytkem KDE.
</p>
<p>
   Rozsáhlé možnosti práce s mailovými zprávami zahrnují mocné filtrování,
   posílání a přijímání zpráv na pozadí (včetně nového rychlého POP3
   proudového zpracování), selektivní stahování zpráv z POP3 serverů
   pomocí filtrovacího mechanismu na hlavičkách zpráv, rychlé stahování
   hlaviček pomocí IMAP (<em>nově</em>), vnořené složky se zprávami, Drag&amp;Drop.
   Další možnosti zahrnují HTML mailové zprávy, třídění zpráv, vložené
   přílohy, automatické doplňování adres, seznamy k distribuci a aliasy (<em>nově</em>)
   a schopnost importovat existující složky zpráv z různých dalších
   populárních klientů pro čtení pošty. Editor zpráv nabízí kontrolu pravopisu,
   undo/redo, vyhledávání a (<em>nově</em>) vylepšený automatický výběr vhodné znakové
   sady pro zprávy.
</p>
<p>
   Pro ochranu soukromí, KMail podporuje OpenPGP šifrování pomoci PGP a
   <a href="http://www.gnupg.org">GnuPG</a>, včetně automatického šifrování
   odchozích zpráv, kdykoliv je to možné ("oportunistické šifrování") (<em>nově</em>).
   Navíc KMail podporuje SLS/TLS pro přístup k POP3 a IMAP4 serverům a
   SMTP a DIGEST-MD5 autentizaci, z čehož většina může být automaticky
   zkonfigurována KMailem (<em>nově</em>). KDE projekt je hrdý na to, 
   že německá spolková vláda 
   <a href="http://www.gnupg.org/aegypten/">sponzoruje KMail</a>
   jako součást svého projektu 
   <a href="http://www.bsi.de/aufgaben/projekte/sphinx/index.htm">Sphinx</a>,
   který má zajistit bezpečné emailovou komunikaci.
</p>
<p>
  <u>Kancelářský balík</u>.
  KDE 3 je dodáváno s <a href="http://www.koffice.org">KOffice</a> 1.1.1,
  integrovaným kancelářským balíkem, který využívá otevřené standardy
  pro komunikaci mezi komponentami a jejich skládání. Všechny komponenty
  KOffice mohou obsahovat jiné komponenty/dokumenty KOffice stejně jako
  obrázky, podporují skenování obrázků, poskytují pokročilé tiskové
  vlastnosti, používají XML pro nativní formáty dokumentů
  a poskytují rozhraní
  <a href="http://developer.kde.org/documentation/library/dcop.html">DCOP</a>
  pro skriptování.
</p>
<p>
  <em>Zpracovávání textu</em>.
  KWord je aplikace pro zpracování textu a publikování ve stylu FrameMakeru,
  ideální pro ty, kdo nepotřebují často vyměňovat dokumenty MS Word s ostatními,
  ačkoliv toto omezení by mělo zmizet s
  <a href="http://developer.kde.org/development-versions/koffice-1.2-release-plan.html">
  uvolněním verze KOffice 1.2</a> v srpnu 2002. KWord je rámově orientovaný,
  umožňující jednoduchý desktop publishing (DTP).
</p>
<p>
  Nastavení odstavce zahrnují okraje, zarovnání, řádkování, odsazení, zarážky,
  přechody na novou stránku a typ písma, styl, barvu a velikost.
  KWord poskytuje návrhář pro úpravu, přidávání a mazání stylů a obsahuje
  množství předdefinovaných stylů.
</p>
<p>
  Vlastnosti na úrovni stránek zahrnují více sloupců na stránku, záhlaví a
  Zápatí (včetně rozdílného pro první stránku) a mnoho přednastavených
  velikostí stránek včetně volitelné.
</p>
<p>
  Vlastnosti na úrovni dokumentů zahrnují tabulku, vkládání textových rámců,
  obrázků a klipartů (soubory .WMF) stejně jako vkládání dalších komponent
  KOffice; šablony, automatické generování obsahu, automatická oprava a kontrola
  pravopisu, číslování kapitol; a proměnné dokumentu, jako např. číslo stránky,
  jméno společnosti, jméno uživatele, datum a čas.
</p>
<p>
  <em>Tabulkový procesor</em>.
  KSpread je skriptovatelný tabulkový procesor poskytující jak tabulkově
  orientované listy tak podporu pro rozsáhlé matematické vzorce a
  statistiky. Mezi schopnosti KSpreadu na úrovni dokumentu patří šablony,
  více tabulek/listů v dokumentu, záhlaví a zápatí, komentáře a odkazy.
</p>
<p>
  KSpread také poskytuje mocnou podporu pro vzorce, zahrnující přes 100
  vzorců (jako např. směrodatná odchylka, rozptyl, současná hodnota), 
  řazení a sekvence (dny týdne, měsíce roku, čísla, atd.).
</p>
<p>
  Schopnosti tabulek/buněk zahrnují kontrolování platnosti pomocí
  konfigurovatelných upozornění/akcí, volitelné obarvování buněk,
  více formátů grafů pro grafické zobrazení dat, nastavování
  řádků a sloupců (velikost, skrytí, druh písma, styl atd.)
  a nastavení buněk (formát data/čísel, přesnost, ohraničení, zarovnání,
  rotace, barva pozadí a vzor, druh písma, styl atd.).
</p>
<p>
  <em>Prezentační program</em>.
  KPresenter je prezentační program. Poskytuje základní operace jako
  vkládání a editace textu (včetně odsazování, řádkování, barev, typů písma
  atd.), vkládání obrázků a klipartů (soubory .WMF) a automatické tvary.
  KPresenter dokáže nastavit mnoho vlastností objektů jako pozadí,
  barevný přechod, druh pera, stín, rotaci a vlastnosti specifické pro typ objektu,
  manipulování objektů, jako změna velikosti, přesouvání, snižování a zvyšování,
  seskupování objektů, a přiřazování efektů pro animování objektů nebo
  měnění stránek.
</p>
<p>
  Na úrovni dokumentu, KPresenter podporuje šablony, záhlaví a zápatí,
  pokročilé undo/redo, nastavování pozadí stránek (barva, přechody,
  obrázky, kliparty, atd.). Poskytuje také prohlížeč struktury prezentace.
</p>
<p>
  Vedle schopnosti generovat prezentace na obrazovce s efekty, dokáže
  KPresenter také generovat HTML prezentace nebo PDF dokumenty jen
  pomocí několika málo kliknutí myší.
</p>
<p>
  <u>PIM</u>.
  <em>Kalendář / Skupinové plánování</em>.
  <a name="korganizer"></a>
  <a href="http://pim.kde.org/components/korganizer.php">KOrganizer</a> je program v KDE
  pro kalendář, plánování a organizování schůzek, seznamů úloh,
  projektů a dalších. Je to integrální část balíku
  <a href="http://pim.kde.org/">KDE PIM</a>,
  který má za cíl být kompletním řešením pro organizování osobních údajů.
  KOrganizer podporuje dva hlavní standardy pro ukládání a výměnu
  kalendářových dat, vCalendar a iCalendar.
</p>
<p>
  Novou vlastností v KDE 3.0 je podpora pro skupinové plánování
  podle iTIP standardu. Toto rozšíření podporuje sdílení schůzek,
  požadavky na střetnutí, odpovědi na požadavky a více.
  Skupinové plánování je založeno na peer-to-peer architektuře
  používající email jako komunikační médium. Spolupracuje
  s ostatními plánovacími aplikacemi podporujícími iTIP standard,
  jako Evolution a Outlook.
</p>
<p>
  KOrganizer v KDE 3.0 má také novou podporu pluginů pro rozšiřování
  KOrganizeru s dalšími na datu založenými informacemi jako prázdniny
  nebo nové pohledy na kalendářová data jako projektový pohled,
  umožňuje propojení kontaktů se schůzkami a úlohami a poskytuje
  inovativní způsob indikace současného data a času ("Marcus Bains
  Line").
</p>
<p>
  <em>Kniha adres</em>.
  <a name="kaddressbook"></a>KDE 3.0 uvádí novou verzi knihovny pro adresář,
  která poskytuje centrální adresář všem KDE aplikacím. Tato nová knihovna
  je založena na vCard standardu a je možné ji rozšířit o LDAP nebo
  databázové servery.
</p>
<p>
  <em>Upomínky</em>.
  <a name="kalarm"></a>
  <a href="http://pim.kde.org/components/kalarm.php">KAlarm</a>
  je nová aplikace v KDE 3.0. Umožňuje rychle nastavit upomínku,
  když není potřeba plná funkcionalita plánovacího programu.
  KAlarm sdílí alarm daemon s KOrganizerem.
</p>
<p>
  <u>Multimédia</u>.
  <a name="multimedia"></a>
  KDE 3 poskytuje bohatou nabídku multimediálních nástrojů,
  od přehrávače CD (KSCD) po média přehrávač pro .WAV, .MP3 a
  <a href="http://www.xiph.org/ogg/vorbis/">OggVorbis</a> audio soubory
  a MPEG-1 a <a href="http://mpeglib.sourceforge.net/">DivX</a>
  video soubory.  Noatun obsahuje audio efekty, šestipásmový grafický ekvalizér,
  architekturu založenou na pluginech, síťovou transparentnost
  a několik skinů. V KDE 3.0 Noatun poprvé nabízí podporu
  pro <a href="http://www.icecast.org/">Icecast</a> a
  <a href="http://www.shoutcast.com/">SHOUTcast</a> digitální
  audio streaming.
</p>
<p>
  Součástí KDE 3 je také aKtion!, video přehrávač podporující
  množství video formátů.
</p>
<p>
  <u>Přístup k souborům, dokumentům a datům</u>.
  KDE poskytuje architekturu pro síťově transparentní přístup
  k souborům, dokumentům a datům pomocí <a name="kioslave">KIOSlave I/O
  objektů</a>. Když je do systému přidán nový KIOSlave, jeho služby jsou
  automaticky přístupné všem KDE aplikacím. Tato modulární
  architektura umožňuje jednoduše přidávat další
  protokoly (jako IPX) ke KDE.
</p>
<p>
  Velké množství protokolů už je implementováno, od HTTP, SFTP/FTP,
  telnet/SSH, POP/IMAP, NFS/SMB/NetBIOS, LDAP, WebDAV (<em>nově</em>)
  a lokálních souborů po manuálové a info stránky, SQL dotazy, audio
  CD, digitální fotoaparáty, PDA a dokonce příkazy shellu. Všechny
  požadavky mohou být přidány do záložek pro rychlý přístup k často
  používaným datům.
</p>
<p>
  <u>Výukový software</u>.
  <a name="edutainment"></a>Balíček výukových programů je novinkou v KDE 3.0
  a je spravován <a href="http://edu.kde.org/">KDE Edutainment projektem</a>,
  který má za cíl vytvářet výukový software založený na KDE.</p>
<p>
  V současné době jsou dostupné tyto aplikace:
  <a href="http://keduca.sourceforge.net/">KEduca</a>, 
  výukový projekt pro vytváření a revizi formulářových testů a zkoušek;
  <a href="http://kgeo.sourceforge.net/">KGeo</a>, interaktivní 
  program pro učení se geometrie podobný Euklid(tm);
  KLettres, hra   pro rozeznávání abecedy a zvuků (ve francouzštině);
  <a href="http://kstars.sourceforge.net/">KStars</a>, grafické desktopové
  planetárium;
  <a href="http://ktouch.sourceforge.net/">KTouch</a>, program pro učení
  se psaní na klávesnici a
  <a href="http://kvoctrain.sourceforge.net/">kvoctrain</a>, program
  pro trénování cizích slovíček.
</p>
<p>
  <u>KDE Kiosk</u>.
  Některá prostředí, jako kiosky, internetové kavárny a podniky požadují,
  aby uživatel neměl plný přístup ke všem možnostem KDE jako prevence
  proti nežádoucím akcím. Pro zajištění těchto potřeb byl vytvořen
  projekt KDE Kiosk.
</p>
<p>
  KDE 3.0 je první verze začínající s tímto novým systémem, což je
  v podstatě systém založený na povolení měnit konfigurační volby
  aplikací. Tento 
  <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/*checkout*/kdelibs/kdecore/R
EADME.kiosk?rev=1.5">systém
  </a> doplňuje konfigurační systém KDE o jednoduché API, které
  mohou aplikace použít pro testování autorizace pro jisté operace.
</p>
<p>
  Jak KDE panel tak správce plochy už používají tento systém,
  a v době vydání KDE 3.1 by už i ostatní hlavní komponenty desktopu,
  jako Konqueror a ovládací centrum, měly používat tuto technologii.
</p>

<h4>
  Vývojové prostředí KDE 3.0
</h4>
<p>
  <u>Knihovny</u>.
  KDE 3.0 nabízí free i proprietárním softwarovým vývojářům vyzrálé,
  mocné a konzistentní API pro rychlý vývoj aplikací. Hlavními mezi
  těmito technologiemi jsou 
  <a href="#DCOP">Desktop COmmunication Protocol (DCOP)</a>, 
  <a href="#KIO">I/O knihovny (KIO)</a>, <a href="#KParts">komponentový
  objektový model (KParts)</a>, <a href="#XMLGUI">GUI založené na XML</a>,
  <a href="#KHTML">standardům vyhovující HTML engine (KHTML)</a>,
  <a href="#aRts">multimediální architektura (aRts)</a> a nové
  databázové třídy.
</p>
<p>
  <em>I/O a virtuální filesystémy</em>.
  <a name="KIO">KIO</a> implementuje I/O pro aplikace pomocí
  tzv. KIOSlave procesů, umožňujících neblokující GUI bez použití
  vláken. KIO je síťově a protokolově transparentní, a tak
  umožňuje přístup k celé škále datových formátů a protokolů
  poskytovaných instalovanými typy <a href="#KIO">KIOSlave</a>.
  KIO také implementuje tzv. trader, který dokáže najít správné KParts
  komponenty pro zadané typy souboru (podle MIME typu); tyto komponenty
  mohou být vloženy do aplikace pomocí technologie KParts.
</p>
<p>
  <em>IPC</em>.
  <a name="DCOP">DCOP</a> je protokol pro komunikaci klient-klient
  přes server pomocí X11 knihovny ICE. Protokol podporuje zasílání
  zpráv i vzdálené volání procedur pomocí brány DCOP XML-RPC.
  KDE 3 rozšiřuje používání DCOP a základní KDE aplikace poskytují
  značnou část funkcionality pro ostatní aplikace. Jsou
  k dispozici DCOP vazby pro jazyky C, C++ a Python, stejně jako
  experimentální vazby pro jazyk Java a pro shell.
</p>
<p>
  <em>Komponenty</em>.
  <a name="KParts">KParts</a> je osvědčený komponentový objektový
  model používaný v KDE. Je používán pro zpracovávání všech aspektů
  skládání komponent, jako umísťování nástrojových lišt a vkládání
  prvků menu na správná místa, když je komponenta aktivována nebo
  deaktivována. KParts dokáže také komunikovat s KIO traderem
  pro nalezení komponenty pro specifické typy souborů nebo
  služeb/protokolů. Tato technologie je značně používána
  v KOffice, KHTML a Konqueroru.
</p>
<p>
  <em>Webové technologie</em>.
  <a name="KHTML"></a>
  <a href="#konq-internet">Internetové vlastnosti Konqueroru</a> jsou
  vlastně odvozeny z <a name="KHTML">KHTML</a>. KHTML je přístupné
  všem aplikacím, jako widget i jako KPart komponenta.
</p>
<p>
  <em>Multimédia</em>.
  Systém multimédií v KDE je založen na
  <a name="aRts"></a><a href="http://www.arts-project.org/">aRts</a>
  a obsahuje přehrávače zmiňované v <a href="#multimedia">
  popisu multimediálního balíku</a>.
  ARts je modulární multimediální systém s plnou podporu streamingu,
  který dokáže vytvářet zvuk a přehrávat audio a video. Příklady
  modulů zahrnují např. filtry, mixery, kodeky, stejně jako moduly
  pro přehrávání zvuku na reproduktorech nebo vzdálené přehrávání
  zvuku přes síť. V KDE 3 má aRts vylepšenou podporu pro nahrávání,
  MIDI a ALSA, a přidanou podporu pro IRIX.
</p>
<p>
  <em>Přístup k databázím</em>.
  KDE 3 poskytuje nové databázově nezávislé API pro přistupování
  k SQL databázím, které zajišťuje podporu pro ODBC stejně jako
  přímou podporu pro Oracle, PostgreSQL a MySQL databáze (další
  ovládače mohou být přidány). Navíc, nové ovládací prvky
  automaticky synchronizují změny mezi databází a GUI.
</p>
<p>
  <em>Dynamické GUI</em>.
  <a name="XMLGUI">
  XML GUI systém</a> využívá XML pro vytváření a umísťování menu,
  nástrojových lišt a dalších prvků GUI. Tato technologie
  nabízí vývojářům a uživatelům výhodu zjednodušené konfigurace
  prvků uživatelského rozhraní, stejně jako jednoduché
  a automatické vyhovování 
  <a href="http://developer.kde.org/documentation/standards/kde/style/basics/index.html">
  KDE GUI standardům</a>, bez ohledu na změny standardu.
</p>
<p>
  <em>Regulární výrazy</em>.
  KDE 3 konečně nabízí novou mocnou třídu pro regulární výrazy.
  Ačkoliv jsou plně kompatibilní a stejně mocné jako regulární výrazy
  <a href="http://www.perl.com/">v Perlu</a>, třídy pro regulární
  výrazy v Qt navíc poskytují plnou podporu pro mezinárodní
  (Unicode) znakové sady.
</p>
<p>
  <u>IDE</u>.
  <a name="kdevelop">KDevelop</a> je vedoucí Linuxové free vývojové
  prostředí s mnoha vlastnostmi pro rychlý vývoj aplikací, včetně
  průvodce vytváření projektu, grafické vytváření dialogů
  (s <a href="http://www.trolltech.com/products/qt/tools.html">Qt Designerem</a>),
  integrované ladění aplikací (volitelně pomocí <a href="#kdbg">KDbg</a>),
  správy projektu, dokumentace a překladů (s <a href="#kbabel">KBabel</a>),
  zabudované podpory pro CVS, textové konzole, zobrazování manuálových
  stránek, vysvěcování syntaxe a množství šablon pro projekty, včetně
  šablon pro ovládací moduly, appletů pro panel, KIOSlaves, pluginy
  pro Konqueror a další.
</p>
<p>
  S KDE 3.0 získal KDevelop výhodu v značně vylepšeném Qt Designeru,
  který nyní vedle tvorby dialogů podporuje i interaktivní vytváření
  hlavního okna aplikace s menu a nástrojovými lištami. Podporuje KDE,
  Qt a vlastní widgety (grafické prvky) a dobře se integruje
  do KDevelopu.
</p>
<p>
  Dále je v KDevelopu přidáno automatické doplňování pro
  věci jako metody tříd a parametry funkcí, plná podpora
  cross-compilingu s možností zadat různé překladače, různé
  volby překladače, cílovou architekturu, včetně podpory
  pro projekty Qt/Embedded (jako
  <a href="http://developer.sharpsec.com/">Zaurus</a> a
  <a href="http://www.compaq.com/products/iPAQ/">iPAQ</a>).
</p>
<p>
  <u>Další vývojové nástroje</u>.
  Další vývojové nástroje v KDE zahrnují:
</p>
<ul>
  <li>
    <a name="kate"></a><a href="http://kate.sourceforge.net/">Kate</a>,
    vícepohledový programátorský editor s vysvěcováním syntaxe a integrovanou
    textovou konzolí.
  </li>
  <li>
    <a name="kdbg"></a><a href="http://members.nextra.at/johsixt/kdbg.html">KDbg</a>,
    frontend k <a href="http://www.gnu.org/directory/gdb.html">gdb</a>
    (<a href="http://www.gnu.cz/">GNU</a> debugger), který lze
    integrovat do KDevelopu, a poskytuje intuitivní prostředí
    pro nastavování breakpointů, zkoumání proměnných a krokování kódu.
  </li>
  <li>
    <a name="kbabel"></a><a href="http://i18n.kde.org/tools/kbabel/">KBabel</a>, pokročilý
    a jednoduše použitelný editor pro překládání aplikací do jiných jazyků,
    který lze také integrovat do KDevelopu, a mezi
    <a href="http://i18n.kde.org/tools/kbabel/features.html">vlastnosti</a>
    patří úplná navigace, plné možnosti editace, vyhledávání, kontrola
    syntaxe a statistické funkce.
  </li>
  <li>Konsole, terminálový emulátor pro KDE, byl pro KDE 3.0 vylepšen v mnoha
    ohledech, včetně podpory klávesových zkratek, přejmenování relací,
    Drag&amp;Drop pro text, možnost přiřazení relací k pracovním adresářům
    a DCOP interface.
  </li>
</ul>
<p>
  <u>Vazby pro programovací jazyky</u>.
  K dispozici jsou vazby pro různé jazyky pro KDE. Je k dispozici plná
  podpora pro C, Objective C, Javu a Python, jejichž vazby se chovají
  identicky jako C++ verze. Navíc jsou k dispozici C# vazby pro Qt 3
  a jsou plánovány pro KDE.
</p>
<p>
  <u>Integrace Qt</u>.
  KDE 3 vylepšuje integrací čistých Qt aplikací do KDE aplikováním
  stylových pluginů KDE i na Qt aplikace, dále byly styly rozšířeny
  pro podporu většího množství widgetů. Vývojáři mohou používat
  Qt multiplatformově bez obětování konzistentního vzhledu v KDE.
</p>
<p>
  <u>Portování do KDE 3</u>.<a name="porting"></a>
  Protože je KDE 3 většinou kompatibilní s KDE 2 na úrovní zdrojových souborů,
  portování by mělo být většinou relativně jednoduché. I větší
  a komplikované aplikace byly portovány pro KDE 3 během hodin. Instrukce
  pro portování aplikací jsou k dispozici zvlášť pro 
  <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE3PORTING.html">
  knihovny KDE </a> a
  <a href="http://doc.trolltech.com/3.0/porting.html">knihovnu Qt</a>.
  Většina KDE 3 API je stabilní, s výjimkou několika tříd, které
  mají v dokumentaci poznamenáno, že se jejich API může pro KDE 3.1 změnit.
</p>

<h4>
  Instalace binárních balíků KDE 3.0
</h4>
<p>
  <u>Binární balíky</u>.
  Někteří dodavatelé operačních systémů Linux/UNIX laskavě poskytli
  binární balíky KDE 3.0 pro některé verze svých distribucí,
  v ostatních případech byly balíky vytvořeny dobrovolníky.
  Některé z těchto binárních balíků jsou dostupné pro stáhnutí zdarma
  z <a href="http://download.kde.org/stable/kde-3.0/">http</a> nebo
  <a href="/mirrors/ftp.php">ftp</a> zrcadel KDE.
  Další binární balíky, stejně jako novější verze balíků, které jsou
  k dispozici nyní, mohou být k dispozici v následujících týdnech.
</p>
<p>
  Prosím věnujte pozornost tomu, že projekt KDE dává k dispozici tyto balíky
  uživatelům ze svých stránek pouze pro jednoduchost. Tým KDE není
  odpovědný za tyto balíky, protože jsou poskytovány třetí osobou,
  - obvykle, ale ne vždy, distributorem příslušné distribuce - s použitím
  nástrojů, překladačů, verzí knihoven a systémy zajišťování kvality, nad
  kterými nemá tým KDE žádnou kontrolu. Pokud nemůžete najít binární balíky
  pro svůj operační systém nebo nejste spokojeni s jejich kvalitou,
  přečtěte si prosím 
  <a href="/download/packagepolicy.php">KDE Binary Package
  Policy</a> anebo kontaktujte svého dodavatele operačního systému.
</p>
<p>
  <u>Potřebné knihovny/závislosti</u>.
  Potřebné knihovny pro různé binární balíky se liší podle systému,
  na kterém byly vytvořeny. Všimněte si také prosím, že některé balíky
  mohou požadovat novější verzi Qt a dalších knihoven než jsou součástí
  systému (např. LinuxDistro X.Y má jen Qt-3.0.0 ale binární balíky potřebují
  Qt-3.0.3). Pro obecné požadavky na knihovny si prosím přečtěte
  <a href="#source_code-library_requirements">požadované/volitelné knihovny</a>
  níže.
</p>
<p>
  <a name="package_locations"><em>Umístění balíků</em></a>.
  V době tohoto oznámení jsou binární balíky k dispozici pro:
</p>
  <ul>
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>, 
    <a href="http://download.kde.org/stable/3.0/Conectiva/">7.0</a></li>
<li><a href="http://www.freebsd.org/">FreeBSD</a>,
    <a href="http://download.kde.org/stable/3.0/FreeBSD/">4.5-STABLE</a></li>
<li><a href="http://www.linux-mandrake.com/">Mandrake</a>,
    <a href="http://download.kde.org/stable/3.0/Mandrake/">8.0, 8.1, 8.2</a></li>
<li><a href="http://www.redhat.com/">Red Hat</a>,
    <a href="http://download.kde.org/stable/3.0/Red%20Hat/README">README</a></li>
<li><a href="http://www.slackware.org/">Slackware</a>,
    <a href="http://download.kde.org/stable/3.0/Slackware/">8.0</a></li>
<li><a href="http://www.suse.cz/">SuSE</a>,
    <a href="http://download.kde.org/stable/3.0/SuSE/">pro i386, ppc, s390 a Sparc</a></li>
<li><a href="http://www.tru64unix.compaq.com/">Tru64</a>,
    <a href="http://download.kde.org/stable/3.0/Tru64/">odkaz</a></li>
<li><a href="http://www.yellowdoglinux.com/">YellowDog</a>,
    <a href="http://download.kde.org/stable/3.0/YellowDog">2.2</a></li>
  </ul>
<p>
  Více binárních balíků se může objevit během dalších týdnů.
</p>
<h4>
  Překlad KDE 3.0 ze zdrojových souborů
</h4>
<p>
  <a name="source_code-library_requirements"></a><u>Požadované/volitelné knihovny</u>.
  KDE 3.0 požaduje nebo dokáže využívat mnoho knihoven, z nichž většina už by měla
  být nainstalována na většině operačních systémů. Pro podrobný seznam knihoven
  prosím jděte <a href="http://www.kde.org/announcements/announce-3.0.html#source_code-library_requirements">sem</a>.
</p>
<p>
  <u>Požadavky na překladač</u>.
  KDE je navrženo jako víceplatformové a tedy přeložitelné mnoha
  variantami Linux/UNIX překladačů. Nicméně KDE postupuje velmi rychle
  a schopnost nativních překladačů na různých UNIX systémech přeložit
  KDE závisí na uživatelích těchto systémů a jejich 
  <a href="http://bugs.kde.org/">hlášení</a> problémů při překladu
  odpovídajícím vývojářům.
</p>
<p>
  Navíc, C++ podpora v <a href="http://gcc.gnu.org/">gcc/egcs</a>,
  nejpoužívanějším KDE překladači, postupuje rychle a nedávno prošla velkými
  změnami. Jako následek KDE nepůjde správně přeložit se starými verzemi
  gcc ani s většinou nových verzí.
</p>
<p>
  Konkrétně, verze gcc starší než gcc-2.95, jako např. egcs-1.1.2
  nebo gcc-2.7.2, nepřeloží správně některé části KDE 3.0.
  Dále některé části KDE 3.0, jako 
  <a href="#aRts">aRts</a>, se nepřeloží správně neupravenými verzemi
  <a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a>
  3.0.x (tento problém by měl být vyřešen s vydáním gcc-3.1).
  Přestože byly hlášeny úspěšné překlady KDE s tzv. gcc-2.96 a gcc-3.1 (cvs),
  projekt KDE v současné době stále doporučuje používat gcc-2.95-* nebo verzi
  gcc, která byla dodána se stabilní distribucí Linuxu a která byla
  použita k překladu stabilního KDE pro tuto distribuci.
</p>
<p>
  <a name="source_code"></a><u>Zdrojové kódy/SRPM</u>.
  Kompletní zdrojové kódy pro KDE 3.0 jsou k dispozici pro
  <a href="http://download.kde.org/stable/kde-3.0/src/">stažení</a>.
</p>

<h4>
  Sponzorování KDE
</h4>
<p>
  Vedle neocenitelného úsilí samotných
  <a href="http://www.kde.org/gallery/index.html">vývojářů KDE</a>,
  významnou podporu pro vývoj KDE poskytli
  <a href="http://www.mandrake.cz/">MandrakeSoft</a> a
  <a href="http://www.suse.cz/">SuSE</a>. Dále
  členové <a href="http://www.kdeleague.org/">Ligy KDE</a>,
  stejně jako <a href="http://www.kde.org/donations.html">
  individuální sponzoři</a> (<a href="/helping/index.php#money">sponzorovat</a>),
  poskytují KDE významnou podporu. Zvláštní poděkování náleží
  <a href="http://www.uni-tuebingen.de/">Univerzitě v T&uuml;bingenu</a>
  za hostování KDE projektu celé ty roky. Děkujeme!
</p>
<h4>
  O prostředí KDE
</h4>
<p>
  KDE je nezávislý kolaborativní projekt stovek vývojářů na celém světě,
  jenž usiluje o vytvoření sofistikovaného, přizpůsobitelného a stabilního
  pracovního prostředí, postaveném na komponentách a síťově transparentní
  architektuře. KDE je živoucím důkazem OpenSource softwaru, založeném na vývojovém
  modelu ve stylu "Bazaar", který vytváří prvotřídní software - porovnatelný,
  a v mnoho ohledech převyšující komplexní komerční software. 
  Chcete-li získat více informací o KDE, prosím navštivte
  <a href="/">webové stránky KDE</a>.
</p>
<a name="links"></a>
<table border="0" cellpadding="16" cellspacing="0">
  <tr valign="top">
    <td>
      <em>Obecné</em>
      <ul>
        <li><a href="/whatiskde/">Co je KDE</a>
        </li>
        <li>
          <a href="/helping/">KDE FAQ (často kladené otázky)</a>
        </li>
        <li>
          <a href="http://bugs.kde.org/">Hlášení chyb a požadování
          nových vlastností</a>
        </li>
        <li>
          <a href="http://www.kde.org/family.html">Rodina stránek</a>
        </li>
        <li>Stažení (<a href="http://download.kde.org/">http</a>,
          <a href="/mirrors/ftp.php">ftp</a>)
        </li>
        <li>
          Další <a href="http://apps.kde.com/">aplikace</a>
        </li>
        <li>
          <a href="/mailinglists/">Emailové konference</a>
        </li>
        <li>
          <a href="http://enterprise.kde.org/">KDE :: Enterprise</a>
        </li>
        <li>
          <a href="http://www.kdeleague.org/">Liga KDE</a>
        </li>
      </ul>
    </td>
    <td>
      <em>Podpora</em>
      <ul>
        <li><a href="/helping/index.php#money">Peněžní</a>
        </li>
        <li>
          <a href="/helping/index.php#code">Vývoj</a>
        </li>
        <li>
          <a href="/helping/index.php#trans">Překlady / Dokumentace</a>
        </li>
        <li>
          <a href="/helping/index.php#art">Grafika / Ikony</a>
        </li>
        <li>
          <a href="http://promo.kde.org/">Propagace</a>
        </li>
      </ul>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <em>KDE 3</em>
      <ul>
        <li><a href="/info/">Informace o KDE 3</a>
        </li>
        <li>KDE 3 obrázky <a href="http://www.kde.org/screenshots/kde300shots.php">tady</a>
          a <a href="/screenshots/">tady</a>
        </li>
        <li>
          <a href="http://www.kde.org/kde2-and-kde3.html">Instrukce</a>
          pro nastavení KDE 2 a KDE 3 systémů současně.
        </li>
        <li>Plán pro <a href="http://developer.kde.org/development-versions/kde-3.1-features.html">KDE 3.1</a>
        </li>
        <li>
          <a href="http://developer.kde.org">Informace pro vývojáře</a>
        </li>
      </ul>
    </td>
    <td>
      <em>Hlavní projekty</em>
      <ul>
        <li><a href="http://www.kdevelop.org/">KDevelop</a>
        </li>
        <li>
          <a href="http://kmail.kde.org/">KMail</a>
        </li>
        <li>
          <a href="http://www.koffice.org/">KOffice</a>
        </li>
        <li>
          <a href="http://konqueror.kde.org/">Konqueror</a>
        </li>
        <li>
          <a href="http://multimedia.kde.org/">Multimédia</a>
        </li>
        <li>
          <a href="http://pim.kde.org/">PIM</a>
        </li>
        <li>
          <a href="http://edu.kde.org/">Edutainment</a>
        </li>
        <li>
          <a href="http://i18n.kde.org/">Překlady</a>
        </li>
      </ul>
    </td>
  </tr>
</table>
<hr noshade="noshade" size="1" width="98%" align="center" />
<p>
  <font size="2">
  <em>Tiskové oznámení</em>: Napsal <a href="mailto:pour at kde.org">Andreas
  Pour</a> pro <a href="http://www.kdeleague.org/">Ligu KDE</a>, s neocenitelnou
  pomocí mnoha dobrovolníků z projektu KDE.
  </font>
</p>
<p>
  <font size="2">
  KDE, K Desktop Environment, KDevelop a KOffice jsou ochranné známky KDE e.V.<br />
  Veškeré ostatní ochranné známky v tomto textu jsou majetkem svých vlastníků.
  </font>
</p>
<hr noshade="noshade" size="1" width="98%" align="center" />
<table border="0" cellpadding="8" cellspacing="0">
  <tr>
    <th colspan="2" align="left">
      Tiskové kontakty:
    </th>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Spojené &nbsp;státy:
    </td>
    <td nowrap="nowrap">
      Andreas Pour<br />
      KDE League, Inc.<br />
      pour@kdeleague.org<br />
      (1) 917 312 3122
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Evropa (francouzsky a anglicky):
    </td>
    <td nowrap="nowrap">
      David Faure<br />
      faure@kde.org<br />
      (33) 4 3250 1445
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Evropa (německy a anglicky):
    </td>
    <td nowrap="nowrap">
      Ralf Nolden<br />
      nolden@kde.org<br />
      (49) 2421 502758
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Evropa (česky, anglicky, francouzsky):
    </td>
    <td nowrap="nowrap">
      Lukáš Tinkl<br />
      lukas@kde.org<br />
    </td>
  </tr>
</table>

<?php include_once("$site_root/media/includes/footer.inc"); ?>

