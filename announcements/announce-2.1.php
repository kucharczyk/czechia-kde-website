<?php
  $site_onemenu = true;
  $site_root = "../";
  $site_title = "Vychází KDE 2.1";
  include_once ("$site_root/media/includes/header.inc");
?>

<h2 align="center">Nová verze KDE připravena pro podnikovou sféru</h2>
<p><strong>Vychází nová stabilní verze KDE, předního prostředí
pro Linux a ostatní Unixy</strong></p>

<p>27. únor, 2001.
<a class="cs" href="/">Tým KDE</a> dnes oznámil
vydání KDE 2.1, novou verzi svého mocného, modulárního a internetového prostředí
pracovní plochy. KDE obsahuje Konqueror,
prvotřídní webový prohlížeč, integrovaný jako součást uživatelsky přívětivé pracovní
plochy; stejně jako KDevelop, pokročilé
prostředí pro vývoj aplikací. Tato verze znamená další velký posun ve stabilitě,
použitelnosti a je vhodná i pro průmyslové rozšíření.
Tým KDE všem uživatelům doporučuje aktualizaci na KDE 2.1.
</p>
<p>
Celé KDE 2.1 je k dispozici pod licencemi Open Source ze
<a class="en" href="http://download.kde.org/stable/2.1/">serveru</a> KDE a jeho
<a class="en" href="/mirrors/ftp.php">zrcadel</a>.
KDE 2.1 zahrnuje hlavní knihovny a pracovní plochu KDE,
(včetně <a class="cs" href="http://www.konqueror.org">Konqueroru</a>),
vývojářské balíky (včetně <a class="en" href="http://www.kdevelop.org">KDevelop</a>) a přes stovku aplikací z
ostatních základních balíků KDE: Administrace, Hry,
Grafika, Multimédia, Síť, Osobní správa informací (PIM), Hračky a Nástroje.
KDE 2.1 je nyní dostupné v
<a class="en" href="http://i18n.kde.org/teams/distributed.html">33 jazycích</a> (pozn. překl.: včetně
<a class="en" href="http://kde-czech.sourceforge.net">češtiny</a>),
v následujících týdnech přibude několik dalších.
</p>
<p>
"Tato druhá velká verze série KDE 2 je opravdovým zlepšením ve smyslu stability,
výkonu a vlastností", sdělil David Faure, vývojář KDE firmy
<a class="en" href="http://www.mandrakesoft.com/">MandrakeSoft</a> a release manager KDE.
"KDE 2 nyní dospěla a každý, kdo používá KDE 1.x, by měl opravdu zvážit aktualizaci.
Konqueror se stal kompletním a robustním webovým prohlížečem a důležité aplikace,
jako poštovní klient (KMail) anebo klient diskuzních skupin (KNode), byli taktéž v mnohém vylepšeny.
Na straně multimédií se poprvé představuje Noatun, nový přehrávač multimédií s
modulárním designem pro přehrávání aktuálních audio a video formátů.
Pro vývoj aplikací se s KDE 2.1 dodává KDevelop, vynikající RAD prostředí,
s kterým se budou cítit doma i programátoři se zkušenostmi s vývojem pro Windows.
KDE 2.1 je zkrátka zásadním počinem v oblasti pracovního a vývojového prostředí
pro Linux/Unix, na cestě za významným postavením v průmyslu, ale i na domácích počítačích."
</p>
<p>
"KDE 2.1 otevírá dveře širokému přijetí linuxových pracovních stanic velkými podniky
a pomůže zajistit Linuxu úspěch na pracovních stanicích, kterého se mu již dostalo na
straně serverů", dodal Dirk Hohndel, CTO <a class="en" href="http://www.suse.com/">SuSE Linux AG</a>.
"S jeho intuitivním rozhraním, vyzrálostí a excelentními prostředky pro vývoj jsem si
jistý, že si podniky a další vývojáři uvědomí enormní potenciál, který KDE nabízí,
a převedou své aplikace a pracovní stanice na Linux/KDE."
</p>
<p>
"KDE razí perfektní grafický design a robustní funkcionalitu", řekla
Sheila Harnett, Senior Technical Staff Member z Linuxového technologického
centra IBM. "KDE 2.1 znatelně posouvá ostatním laťku ve funkčnosti, použitelnosti a
kvalitě prakticky ve všech ohledech pracovní plochy".
</p>
<p>
"S uvedením verze 2.1 KDE opět předvedlo svou schopnost nabídnout
bohatý software a zajistit kompletní a stabilní prostředí pro každodenní
používání", dodal Ga&euml;l Duval, spoluzakladatel společnosti
MandrakeSoft. "Tato poslední verze vydláždila KDE cestu k pracovní ploše
uživatelů v podnicích i doma. Od plnohodnotného webového prohlížeče
až po uživatelsky příjemné Ovládací centrum, KDE zajišťuje veškeré
běžné prostředky, které mnozí uživatelé potřebují, aby opustili Windows
nadobro."
</p>
<p>
"Jsme nadšeni vylepšeními v KDE 2.1 a budeme potěšeni nadále přispívat
do tohoto projektu", řekl Rene Schmidt, výkonný viceprezident firmy Corel pro
linuxové produkty. "KDE se nadále zlepšuje s každou verzí a tato vylepšení
pomohou naší lehce použitelné distribuci stát se ještě dokonalejší než dosud."
</p>
<p>
"Větší počet a dostupnost aplikací pro Linux je důležitým faktorem, který určí, zda
se Linux prosadí i na podnikové pracovní stanice", řekl Drew Spencer,
CTO Caldera Systems, Inc. "KDE 2.1 se touto problematikou zabývá integrací
prohlížeče Konqueror a prostředí KDevelop, nástroje, který umožňuje vývojářům
vytvářet aplikace v C++ pro různá prostředí. Spolu s již existujícími nástroji pro KDE je
KDevelop komplexním řešením pro vývojáře."
</p>
<p>
<strong><em>KDE 2:  K Desktop Environment</em></strong>.
<a name="Konqueror"></a>
<a class="cs" href="http://www.konqueror.org">Konqueror</a>
je prohlížeč WWW nové generace, správce souborů a prohlížeč dokumentů.
Konqueror je plně kompatibilní se
standardy, má komponentově orientovanou architekturu, která kombinuje funkčnost
aplikací jako Internet Explorer, Netscape Communicator a Windows Explorer.
Konqueror podporuje celou řadu současných internetových standardů, jako jsou
JavaScript, Java, XML, HTML 4.0, CSS-1 a CSS-2 (kaskádové styly), SSL (Secure Socket
Layer pro bezpečnou komunikaci) a zásuvné moduly aplikace Netscape Communicator
(pro přehrávání technologií jako Flash, RealAudio, RealVideo a podobně).
</p>
<p>
Kromě toho síťová transparentnost knihoven KIO poskytuje podporu pro přístup
nebo prohlížení souborů na Linuxu, přes NFS, sdílené prostředky SMB MS Windows,
HTTP stránky, FTP anebo LDAP adresáře. Tato modulární souborová architektura
zjednodušuje přidávání dalších protokolů (jako např. IPX nebo WebDAV) do KDE, a ty
budou pak automaticky dostupné všem aplikacím KDE.
</p>
<p>
<a name="customizability">Přizpůsobitelnost a konfigurovatelnost</a>
KDE se dotýká všech aspektů tohoto pracovního prostředí.
Propracovaná podpora témat v KDE začíná u stylů Qt, který
umožňuje vývojářům a designerům vytvořit si svoje vlastní témata ovládacích
prvků. KDE je dodáván s více než 14 těmito styly; některé z nich emulují vzhled
různých operačních systémů a navíc je podporován i import témat GTK a GNOME.
Navíc KDE 2 plně podporuje Unicode a BIDI (jazyky jako Arabština
a Hebrejština) a KHTML je jediné volně dostupné renderovací jádro, které zvládá tyto
vlastnosti na platformě X11/Linux.
</p>
<p>
KDE 2 obsahuje pokročilou multimediální síťově transparentní
architekturu založenou na <a name="arts">aRts</a>,
Analog Realtime Synthesizer. Arts umožňuje současné přehrávání několika audio a
video proudů, ať už lokálně nebo po síti. Arts je plnohodnotný zvukový systém,
který zahrnuje různé efekty a filtry, modulární analogový syntezátor a směšovač.
Zvukový server aRts zajišťuje síťově transparentní podporu zvuku jak pro vstup, tak
i pro výstup pomocí MCOP, architektury na bázi CORBA, a umožňuje tak aplikacím běžícím na
vzdálených počítačích výstup zvuku spolu se vstupem z lokální stanice. Tato architektura
přináší velice potřebný doplněk k síťové transparentnosti X a poprvé umožňuje
uživatelům spouštět vzdálené aplikace se zapnutým zvukem. Navíc aRts umožňuje výstup
zvuku anebo videa z několika aplikací (lokálních i vzdálených) naráz. Podpora videa je
<a class="en" href="http://mpeglib.sourceforge.net/">dostupná</a>
pro formáty MPEG 1, 2 a 4 (experimentální), jakožto pro AVI a DivX.
</p>
<p>
Kromě vyjímečné podpory Internetu a standardů pro sdílení souborů zmíněných
<a class="en" href="#Konqueror">výše</a>,
KDE 2 je plně kompatibilní s dostupnými desktopovými standardy pro Linux. KWin, nový
správce oken KDE vyhovuje nové specifikaci pro správce oken
<a class="en" href="http://www.freedesktop.org/standards/wm-spec.html">Window Manager Specification</a>.
Konqueror a celé KDE vyhovují standardu pro soubory pracovní plochy
<a class="en" href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/kio/DESKTOP_ENTRY_STANDARD">
Desktop Entry Standard</a>.  Konqueror využívá standardu
<a class="en" href="http://pyxml.sourceforge.net/topics/xbel/docs/html/">XBEL</a> pro ukládání
a výměnu záložek. Dále je celé KDE plně kompatibilní s protokolem
<a class="en" href="http://www.newplanetsoftware.com/xdnd/">X Drag-and-Drop (XDND)</a>
a s protokolem správy relace X11R6 (XSMP).
</p>
<p>
<strong><em>KDE 2: K Development Environment</em></strong>.
KDE 2.1 nabízí vývojářům propracované vývojové prostředí (IDE), ale i bohatou sadu
technologických vylepšení nad kritiky oceňovanou sérií KDE 1. K nim patří hlavně
<a class="en" href="#DCOP">Desktop COmmunication Protocol (DCOP)</a>,
<a class="en" href="#KIO-devel">I/O knihovny (KIO)</a>, <a class="en" href="#KParts">objektový komponentový
model (KParts)</a>, třídy pro tvorbu <a class="en" href="#XML_GUI">GUI postavené na XML</a>
a standardy podporující <a class="en" href="#KHTML">renderovací jádro (KHTML)</a>.
</p>
<p>
KDevelop je předním linuxovým
vývojovým prostředím (IDE) -- zahrnuje návrh dialogů, integrované ladění, správu
projektu, dokumentační a překladové nástroje, vestavěnou podporu týmového vývoje
a mnoho dalších vlastností.
</p>
<p>
<a name="KParts">KParts</a>, komponentový objektový model KDE 2,
umožňuje aplikacím vnořit do
sebe část nebo dokument jiné aplikace. Tato technologie zvládá veškeré aspekty
takovéhoto "zanořování", včetně umístění nástrojových lišt, správné slučování
nabídek a (de-)aktivaci daného dokumentu. KParts rovněž spolupracuje s KIO
traderem při vyhledávání dostupných ovladačů pro daný MIME typ, protokol nebo
službu. Tato technologie je široce využívána v sadě
<a class="cs" href="http://www.koffice.org">KOffice</a> a v
aplikaci
Konqueror.
</p>
<p>
<a name="KIO-devel">KIO</a> implementuje vstupně-výstupní operace v odděleném procesu, a umožňuje
tak aplikacím vytvořit neblokované GUI bez použití vláken. Tato třída je síťově
transparentní, a tudíž může být použita pro přístup k lokálním souborům, stejně
jako přes protokoly HTTP, FTP, POP, IMAP, NFS, SMB a LDAP. Tato modulární
souborová architektura KDE navíc zjednodušuje přidávání dalších protokolů (jako
např. WebDAV), a ty budou pak automaticky dostupné všem aplikacím KDE. KIO
implementuje službu typu "trader", která umožňuje vyhledávání specifických MIME
typů, a ta může být poté začleněna do požadující aplikace pomocí technologie KParts.
</p>
<p>
<a name="XML_GUI">XML GUI</a> využívá XML k vytváření a umísťování
nabídek, nástrojových lišt a v budoucnu i
dalších aspektů uživatelského rozhraní. Tato technologie nabízí vývojářům a
uživatelům výhodu jednoduché přizpůsobitelnosti GUI a automatickou kompatibilitu
se <a class="en" href="http://developer.kde.org/documentation/standards/">standardy KDE</a>,
nezávisle na modifikaci těchto standardů.
</p>
<p>
<a name="DCOP">DCOP</a> je komunikační protokol typu klient-klient, předávaný serverem
pomocí standardní X11 knihovny ICE. Protokol podporuje jak předávání zpráv,
tak vzdálené volání procedur pomocí brány XML-RPC &lt;-&gt; DCOP. Dostupné jsou
vazby pro C, C++ a Python, spolu se zatím experimentálními vazbami pro Javu.
</p>
<p>
<a name="KHTML">KHTML</a> je renderovací a vykreslovací jádro plně kompatibilní s HTML 4.0
Tato třída podporuje celou řadu současných internetových standardů, jako jsou
JavaScript, Java, CSS-1 a CSS-2 (kaskádové styly), SSL (Secure Socket Layer
pro bezpečnou komunikaci) a zásuvné moduly aplikace Netscape Communicator
(pro přehrávání technologií jako Flash, RealAudio, RealVideo a podobně).
Třída KHTML může být jednoduše využita v jiné aplikace buďto jako normální widget
(standardními mechanismy X Window), tak jako komponenta (pomocí technologie KParts).
Na druhé straně má KHTML schopnost pohltit do sebe jinou komponentu pomocí již
zmiňované technologie KParts.
</p>
<h3>Získání a kompilace KDE 2.1</h3>
<p>
Zdrojové balíky pro KDE 2.1 jsou volně dostupné na
<a class="en" href="http://download.kde.org/stable/2.1/">ftp.kde.org/stable/2.1/</a>
nebo v ekvivalentním adresáři na jednom z mnoha
<a class="en" href="/mirrors/ftp.php">zrcadel</a> FTP serveru KDE.
KDE 2.1 vyžaduje qt-2.2.4, které je dostupné na výše uvedených místech pod jménem qt-x11-2.2.4.tar.gz.
KDE 2.1 by mělo fungovat s Qt-2.2.3, ale doporučuje se verze 2.2.4.
</p>
<p>
Další informace ohledně kompilace a instalace KDE 2.1 najdete v
<a class="cs" href="/download/">instalačních
pokynech</a>;
narazíte-li na problémy při kompilaci, podívejte se prosím na odpovídající
<a class="en" href="http://www.kde.org/compilationfaq.html">často kladené dotazy</a>.
</p>
<h3>Instalace binárních balíčků</h3>
<p>
Někteří distributoři dodávají binární balíčky KDE pro jisté verze svých distribucí.
Některé z těchto binárních balíčků KDE 2.1 budou volně dostupné ke stažení na
<a class="en" href="ftp://ftp.kde.org/pub/kde/stable/2.1/distribution/">ftp://ftp.kde.org/pub/kde/stable/2.1/distribution/</a>
nebo v příslušném adresáři na některém z
<a class="en" href="/mirrors/ftp.php">mirror</a> serverů. Prosím všimněte si,
že tým KDE není zodpovědný za tyto balíčky, jelikož jsou dodávány třetí stranou
-- typicky, ale ne vždy, dodavatelem konkrétní distribuce.
</p>
<p>
KDE 2.1 vyžaduje qt-2.2.4, které je dostupné na výše uvedených místech pod jménem
qt-x11-2.2.4.tar.gz. KDE 2.1 by mělo fungovat s Qt-2.2.3, ale doporučuje se verze 2.2.4.
</p>
<p>
V době vydání jsou k dispozici binární balíčky pro tyto distribuce:
</p>

<ul>
<li> <a class="en" href="http://download.kde.org/stable/2.1/">Caldera OpenLinux 2.4 (i386)</a></li>
<li> <a class="en" href="http://download.kde.org/stable/2.1/">Tru64 Systems</a></li>
<li> <a class="en" href="http://download.kde.org/stable/2.1/">FreeBSD</a></li>
<li>Linux-Mandrake 7.2:  <a class="en" href="http://download.kde.org/stable/2.1/">i586</a></li>
<li>RedHat Linux:
<ul>
<li>7.0:  <a class="en" href="http://download.kde.org/stable/2.1//">i386</a>,
  <a class="en" href="http://download.kde.org/stable/2.1/">Alpha</a>,
  <a class="en" href="http://download.kde.org/stable/2.1/">IA64</a>,
  <a class="en" href="http://download.kde.org/stable/2.1/">Sparc</a> a
  <a class="en" href="http://download.kde.org/stable/2.1/">S390</a></li>
<li>6.x:  <a class="en" href="http://download.kde.org/stable/2.1/">i386</a>,
  <a class="en" href="http://download.kde.org/stable/2.1/">Alpha</a> a
  <a class="en" href="http://download.kde.org/stable/2.1/">Sparc</a></li>
</ul></li>
<li>SuSE Linux:
<ul>
  <li>7.1:  <a class="en" href="http://download.kde.org/stable/2.1/">i386</a> a
    <a class="en" href="http://download.kde.org/stable/2.1/">Sparc</a>; dále také
    <a class="en" href="http://download.kde.org/stable/2.1/">i18n</a> nebo
    <a class="en" href="http://download.kde.org/stable/2.1/">noarch</a> adresář se společnými soubory</li>
  <li>7.0:  <a class="en" href="http://download.kde.org/stable/2.1/">i386</a> a
    <a class="en" href="http://download.kde.org/stable/2.1/">PPC</a>; dále také
    <a class="en" href="http://download.kde.org/stable/2.1/">i18n</a> nebo
    <a class="en" href="http://download.kde.org/stable/2.1/">noarch</a> adresář se společnými soubory</li>
  <li>6.4:  <a class="en" href="http://download.kde.org/stable/2.1/">i386</a>; dále také
    <a class="en" href="http://download.kde.org/stable/2.1/">i18n</a> nebo
    <a class="en" href="http://download.kde.org/stable/2.1/">noarch</a> adresář se společnými soubory</li>
  <li>6.3:  <a class="en" href="http://download.kde.org/stable/2.1/">i386</a>; dále také
    <a class="en" href="http://download.kde.org/stable/2.1/">i18n</a> nebo
    <a class="en" href="http://download.kde.org/stable/2.1/">noarch</a> adresář se společnými soubory</li>
</ul></li>
</ul>
<p>
Binární balíčky pro další distribuce budou k dispozici v následujících dnech a
týdnech. Prosím navštivte opět naše FTP servery pro kontrolu.
</p>
<h3>O prostředí KDE</h3>
<p>
KDE je nezávislý kolaborativní projekt stovek vývojářů na celém světě,
jenž usiluje o vytvoření sofistikovaného, přizpůsobitelného a stabilního
pracovního prostředí, postaveném na komponentách a síťově transparentní
architektuře. KDE je živoucím důkazem OpenSource softwaru, založeném na
vývojovém modelu ve stylu "Bazaar", který vytváří prvotřídní software --
porovnatelný, a v mnoho ohledech převyšující komplexní komerční software.
</p>
<p>
Chcete-li získat více informací o KDE, prosím navštivte
<a class="cs" href="/whatiskde/">webové stránky
KDE</a>. Více informací
o KDE 2 je dostupných na těchto dvou
(<a class="en" href="http://devel-home.kde.org/~granroth/LWE2000/">1</a>,
<a class="en" href="http://www.mandrakesoft.com/">2</a>) prezentacích, včetně
vyvíjejícího se <a class="en" href="http://www.kde.org/documentation/faq/index.html">FAQ</a>,
velkého počtu <a  class="cs" href="/screenshots/">screenshotů</a>,
informací pro <a class="en" href="http://developer.kde.org/documentation/kde2arch/index.html">vývojáře</a> a průvodce
převodem aplikací z
<a class="en" href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html?rev=2.5">KDE 1 na KDE 2</a>.
</p>
<hr width="90%" align="center" />
<p>Veškeré ochranné známky v tomto textu jsou majetkem svých vlastníků.<br />
[český překlad Lukáš Tinkl &lt;<a class="en" href="mailto:lukas@suse.cz">lukas@suse.cz</a>&gt;]</p>
<hr width="90%" align="center" />
<table border="0" cellpadding="8" cellspacing="0">
<tr><th colspan="2" align="left">
Tiskové kontakty:
</th></tr>
<tr valign="top"><td align="right">
USA:
</td><td>
Kurt Granroth<br />
granroth@kde.org<br />
(1) 480 732 1752<br />&nbsp;<br />
Andreas Pour<br />
pour@kde.org<br />
(1) 718 456 1165
</td></tr>
<tr valign="top"><td align="right">
Evropa <br /> (francouzština a angličtina):
</td><td>
David Faure<br />
faure@kde.org<br />
(44) 1225 837409
</td></tr>
<tr valign="top"><td align="right">
Evropa <br /> (angličtina a němčina):
</td><td>
Martin Konold<br />
konold@kde.org<br />
(49) 179 2252249
</td></tr>
<tr valign="top"><td align="right">
Evropa <br /> (čeština, angličtina a <br /> francouzština):</td>
<td>
Lukáš Tinkl<br />
lukas@kde.org,<br /> 
lukas.tinkl@suse.cz<br />
(420) 2 8309 5381
</td></tr>
</table>

<?php include_once("$site_root/media/includes/footer.inc"); ?>
