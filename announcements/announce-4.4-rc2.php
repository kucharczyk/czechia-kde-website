<?php
  $page_title = "Ohlášení vydání Softwarové kompilace KDE 4.4 RC2";
  $site_root = "../";
  include "header.inc";
?>

<p>PRO OKAMŽITÉ VYDÁNÍ</p>

<!-- // Boilerplate -->

<h3 align="center">
Vydána Softwarová kompilace KDE 4.4 RC2: krycí jméno "CampKDE"
</h3>

<p align="justify">
  <strong>
KDE přináší druhý release candidate nového Desktopu, Aplikací a Vývojářské platformy verze 4.4
</strong>
</p>

<p align="justify">
25. ledna 2010. KDE dnes vydali druhý release candidate další verze Softwarové kompilace KDE (KDE SC). KDE SC 4.4 Release Candidate 1 poskytuje testovací základ pro odhalování chyb v nadcházející Softwarové kompilaci KDE, s komponentami KDE Plasma Workspaces, Aplikace poháněné KDE a Vývojářskou platformou KDE.<br />
Seznam změn mezi 4.3 a 4.4 je obzvlášť dlouhý. Podstatné změny mohou být pozorovány všude:
<p>
<ul>
    <li>Framework <strong>Významové vyhledávání Nepomuk</strong> has made leaps: Nový storage backend framework podstatně zrychlil. S KDE 4.4.0 jsou dodávána nová uživatelská rozhraní pro interakci s databází Nepomuku. Díky pohledu časové osy vašich souborů snáze naleznete soubory použité v minulosti.</li>
    <li><strong>Plasma Desktop</strong> byl nadále cíděn. Vývojáři a designeři se věnovali mnohým prvkům uživatelského rozhraní. Nový průzkumník widgetů umožňuje bohatší prožitek ze správy desktop widgets. Plasma widgets mohou nyní být přes síť sdíleny s jinými uživateli, ovládání úložných zařízení v desktop shell bylo zefektivněno. V 4.4 také jako technologický náhled debutuje mladší sourozenec Plasmy, Netbook shell. </li>
	<li><strong>Nové aplikace</strong> na obzoru se pohybují od aplikace Blogilo, rich-client blogovacího nástroje k aplikacím <a href="http://edu.kde.org/cantor/">Cantor</a> a <a href="http://edu.kde.org/rocs/">Rocs</a>, dvěma vědeckým aplikacím pro potřeby pokročilé matematiky a teorie grafů. 
Mnoho dalších aplikací, jako prohlížeč obrázků Gwenview nebo správce souborů Dolphin, bylo vylepšeno.</li>
    <li>KDE <strong>Vývojářská platforma</strong> přidává nový autorizační framework KAuth ke snadné a bezpečné eskalaci práv, tisk lichých a sudých stran, podporu skenerů pro platformu Windows a první kousky integrace populárního renderovacího enginu webkit. </li>
</ul>
Toto jsou jen některé nové vlastnosti, které můžete od nové Softwarové kompilace KDE 4.4 očekávat,k dispozici je též delší <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Release_Goals">seznam změn (anglicky)</a>. Dlouhý seznam změn přichází s ještě delším seznamem menších či větších oprav chyb a zlepšení výkonu, které vedou ke znatelnému zlepšení uživatelského prožitku.
</p>
<p>
Vydání je pojmenováno po Camp KDE. Americké konferenci KDE, která se konala minulý týden v San Diegu v Kalifornii.
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="http://kde.org/announcements/announce_4.4-beta1/dolphin-systemsettings.png">
    <img src="http://kde.org/announcements/announce_4.4-beta1/dolphin-systemsettings_thumb.png" alt="KDE SC 4.4 RC2" align="center"  height="405"  />
</a>
    <br />
    <em>Dolphin a Nastavení systému v 4.4</em>
</div>

<p align="justify">
<p />
Více o desktopu a aplikacích KDE 4.4 naleznete v poznámkách k vydáním <a href="http://www.kde.org/announcements/4.3/">KDE 4.3.0</a>,
<a href="http://www.kde.org/announcements/4.2/">KDE 4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">KDE 4.1.0</a> a
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a> , neboť KDE SC 4.4 je dalším krokem ve vývoji postaveným na těchto předchozích vydáních.
<strong>KDE SC 4.4 RC2 není stabilní software a jako takový není vhodný pro každodenní produkční použití.</strong> KDE SC 4.4 RC2 je mířeno na testery a ty, které zajímá, jak bude vypadat příští vydání KDE SC.<br />
Tento release candidate bude 9.února následován první stabilní verzí série KDE SC 4.4, 4.4.0. Poté bude KDE dodávat obvyklé měsíční opravy chyb a aktualizace překladů.
<p />



<p align="justify">
KDE, včetně všech jeho knihoven a jeho aplikací, je dostupné svobodně pod Open Source licencemi. Zdrojové kódy a různé binární formáty můžete získat na <a
href="http://download.kde.org/unstable/4.3.90/">http://download.kde.org</a> či na <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
nebo s jakýmkoliv <a href="http://www.kde.org/download/distributions.php">většinovým GNU/Linux a UNIX systémem</a>, které se dnes vydávají.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="http://kde.org/announcements/announce_4.4-beta1/systemtray-settings.png">
    <img src="http://kde.org/announcements/announce_4.4-beta1/systemtray-settings_thumb.png" alt="KDE SC 4.4 RC2" align="center"  height="405"  />
</a>
    <br />
    <em>Vylepšený system tray v 4.4</em>
</div>


<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="http://kde.org/announcements/announce_4.4-beta1/blogilo.png">
    <img src="http://kde.org/announcements/announce_4.4-beta1/blogilo_thumb.png" alt="KDE SC 4.4 RC2" align="center"  height="405"  />
</a>
    <br />
    <em>Nově v 4.4: Blogovací klient Blogilo</em>
</div>



<!-- // Boilerplate again -->

<h4>
	Instalace binárních balíčků KDE SC 4.4 RC2
</h4>
<p align="justify">
  <em>Balíčkovači</em>.
  Někteří poskytovatelé Linuxu/UNIXu laskavě poskytli binární balíčky KDE SC 4.4 RC2 pro některé verze svých distribucí, v jiných případech tak udělali dobrovolníci. 
  Některé z těchto binárních balíčku jsou dostupné zdarma ke stažení z <a
href="http://download.kde.org/binarydownload.html?url=/unstable/4.3.90/">http://download.kde.org</a>.
  Další balíčky, včetně aktualizací balíčku stávajících, by mohly být dostupné v příštích týdnech.
</p>

<p align="justify">
  <a name="package_locations"><em>Umístění balíčků</em></a>.
	Pro stávající seznam dostupných binárních balíčků o kterých byl Projekt KDE informován navštivte <a href="/info/4.3.90.php">KDE SC 4.4 RC2 Info Page</a>.
</p>

<h4>
  Kompilace KDE SC 4.4 RC2 (a.k.a 4.3.95)
</h4>
<p align="justify">
  <a name="source_code"></a>
	Kompletní zdrojový kód KDE SC 4.4 RC2 je <a
href="http://download.kde.org/unstable/4.3.95/src/">volně ke stažení</a>.
Instrukce ke kompilaci a instalaci KDE SC 4.4 RC2 jsou dostupné na <a href="/info/4.3.95.php#binary">KDE SC 4.3.95 Info Page</a>.
</p>

<h4>
  Podpora KDE
</h4>
<p align="justify">
KDE je <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
komunita, která existuje a roste díky pomoci mnoha dobrovolníků, kteří darují svůj čas a snahu. KDE neustále hledá nové dobrovolníky a přispěvatele, ať už se jedná o pomoc s programováním, opravováním chyb nebo jejich hlášení, psaním dokumentace, překládáním, propagací, penězi atd. Všechny příspěvky jsou vděčně a dychtivě přijímány. Pro další informace čtěte prosím <a
href="/support/">stránku Podpora KDE</a></p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
