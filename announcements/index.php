<?php
  $site_onemenu = true;
  $site_root = "../";
  $site_title = "Tiskové zprávy";
  include_once ("$site_root/media/includes/header.inc");
?>

<h1>Tiskové zprávy</h1>
<p>
Kompletní seznam anglických tiskových zpráv k dispozici <a class="en" href="http://www.kde.org/announcements/">zde</a>.
Poslední tisková zpráva <a href="latest.php">zde</a>.
</p>


<h2>Tiskové zprávy v češtině</h2>

<p><b>3. února 2004</b> - <a class="en" href="announce-3.2.php">Vyšlo KDE 3.2</a>
<br />
<i>"Projekt KDE uvolňuje další verzi třetí řady vedoucího desktopového prostředí pro Linux/UNIX."</i>
</p>

<p><b>3. dubna 2002</b> - <a class="cs" href="announce-3.0.php">Vyšlo KDE 3.0</a>
<br />
<i>"Projekt KDE uvolňuje třetí generaci vedoucího desktopového prostředí pro Linux/UNIX,
    a nabízí tak podnikům, vládám i školám skvělé a otevřené řešení pro 
    pracovní stanice a domácí počítače."</i>
</p>

<p><b>15. srpna 2001</b> - <a class="cs" href="announce-2.2.php">Vyšlo KDE 2.2</a>
<br />
<i>"Tým KDE dnes oznámil vydání KDE 2.2, mocného a snadno použitelného rozhraní s podporou Internetu
    pro Linux a další Unixy. Díky rychlému a ukázněnému vývoji obsahuje KDE 2.2 celou řadu
    zdokonalení týkajících se rychlosti a stability, stejně jako velké množství
    nových vylepšení."</i>
</p>

<p><b>26. února 2001</b> - <a class="cs" href="announce-2.1.php">Vyšlo KDE 2.1</a>
<br />
<i>"KDE obsahuje Konqueror, prvotřídní webový prohlížeč, integrovaný jako
   součást uživatelsky přívětivé pracovní plochy; stejně jako KDevelop,
   pokročilé prostředí pro vývoj aplikací. Tato verze znamená další velký
   posun ve stabilitě, použitelnosti a je vhodná i pro podnikovou sféru."</i>
</p>

<?php include_once("$site_root/media/includes/footer.inc"); ?>
