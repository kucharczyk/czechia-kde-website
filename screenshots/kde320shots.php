<?php
  $page_title = "KDE 3.2 Screenshots";
  $site_root = "../";
  include "header.inc";
?>

<p> Here are some screenshots showing <a
href="../announcements/announce-3.2.php">KDE 3.2</a>, an older version
of the powerful K&nbsp;Desktop&nbsp;Environment, a free desktop environment for
Unix.</p>

<?php
  $gallery = new ImageGallery("These are screenshots of KDE 3.2.x");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot01a.jpg",
  "images/3.2/snapshot01a.png",
  160, 120,
  "KDE splash screen",
  "",
  "KDE splash screen");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot02.jpg",
  "images/3.2/snapshot02.png",
  160, 120,
  "The panel menu with categories titles and prefering generic names",
  "",
  "The panel menu with categories titles and prefering generic names");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot04a.jpg",
  "images/3.2/snapshot04a.png",
  160, 120,
  "Window decoration configuration with preview of selected theme",
  "",
  "Window decoration configuration with preview of selected theme");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot05b.jpg",
  "images/3.2/snapshot05b.png",
  160, 120,
  "Plastik is a newly included clean style",
  "",
  "Plastik is a newly included clean style");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot07d.jpg",
  "images/3.2/snapshot07d.png",
  160, 120,
  "The panel can be configured to be transparent",
  "",
  "The panel can be configured to be transparent");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot09c.jpg",
  "images/3.2/snapshot09c.png",
  160, 120,
  "Rewritten background settings configuration dialogs",
  "",
  "Rewritten background settings configuration dialogs");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot09f.jpg",
  "images/3.2/snapshot09f.png",
  160, 120,
  "KDE 3.2 introduces a new funny looking launch feedback cursor animation",
  "",
  "KDE 3.2 introduces a new funny looking launch feedback cursor animation");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot09h.jpg",
  "images/3.2/snapshot09h.png",
  160, 120,
  "Screen saver categories help to find your favorite",
  "",
  "Screen saver categories help to find your favorite");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot09i.jpg",
  "images/3.2/snapshot09i.png",
  160, 120,
  "Choose between different splash screen styles and themes",
  "",
  "Choose between different splash screen styles and themes");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot09o.jpg",
  "images/3.2/snapshot09o.png",
  160, 120,
  "KHotKeys allows global keyboard shortcuts and mouse gestures",
  "",
  "KHotKeys allows global keyboard shortcuts and mouse gestures");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot09s.jpg",
  "images/3.2/snapshot09s.png",
  160, 120,
  "Define how you want to be notified of events",
  "",
  "Define how you want to be notified of events");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot09u.jpg",
  "images/3.2/snapshot09u.png",
  160, 120,
  "Simple installation of fonts with the font installer",
  "",
  "Simple installation of fonts with the font installer");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot10b.jpg",
  "images/3.2/snapshot10b.png",
  160, 120,
  "Terminal emulator now with real tabs and transparency",
  "",
  "Terminal emulator now with real tabs and transparency");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot11f.jpg",
  "images/3.2/snapshot11f.png",
  160, 120,
  "KDE's file manager with file previews and tooltip",
  "",
  "KDE's file manager with file previews and tooltip");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot11i.jpg",
  "images/3.2/snapshot11i.png",
  160, 120,
  "The sidebar can be within Konqueror or on your desktop",
  "",
  "The sidebar can be within Konqueror or on your desktop");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot11k.jpg",
  "images/3.2/snapshot11k.png",
  160, 120,
  "Different views within Konqueror (disk usage, list view, icon view)",
  "",
  "Different views within Konqueror (disk usage, list view, icon view)");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot11m.jpg",
  "images/3.2/snapshot11m.png",
  160, 120,
  "Konqueror as web browser with enhanced tabs and split view",
  "",
  "Konqueror as web browser with enhanced tabs and split view");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot11o.jpg",
  "images/3.2/snapshot11o.png",
  160, 120,
  "Netscape compatible plugins like Flash are supported",
  "",
  "Netscape compatible plugins like Flash are supported");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot12.jpg",
  "images/3.2/snapshot12.png",
  160, 120,
  "The text editor with syntax highlighting and embedded terminal",
  "",
  "The text editor with syntax highlighting and embedded terminal");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot13.jpg",
  "images/3.2/snapshot13.png",
  160, 120,
  "The new mail client version features many improvements",
  "",
  "The new mail client version features many improvements");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot14a.jpg",
  "images/3.2/snapshot14a.png",
  160, 120,
  "Kontact embeds KMail, KOrganizer, KAddressbook and KNotes into one application",
  "",
  "Kontact embeds KMail, KOrganizer, KAddressbook and KNotes into one application");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot14b.jpg",
  "images/3.2/snapshot14b.png",
  160, 120,
  "KOrganizer running as part of Kontact",
  "",
  "KOrganizer running as part of Kontact");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot15.jpg",
  "images/3.2/snapshot15.png",
  160, 120,
  "KDE includes a bunch of educational applications",
  "",
  "KDE includes a bunch of educational applications");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot16.jpg",
  "images/3.2/snapshot16.png",
  160, 120,
  "Over 30 games are included",
  "",
  "Over 30 games are included");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot19.jpg",
  "images/3.2/snapshot19.png",
  160, 120,
  "The youngest are not forgotten",
  "",
  "The youngest are not forgotten");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot21.jpg",
  "images/3.2/snapshot21.png",
  160, 120,
  "KDE's scanning frontend",
  "",
  "KDE's scanning frontend");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot22.jpg",
  "images/3.2/snapshot22.png",
  160, 120,
  "An overview of graphical utilities",
  "",
  "An overview of graphical utilities");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot23.jpg",
  "images/3.2/snapshot23.png",
  160, 120,
  "Kopete is the new multi-protocol instant messenger",
  "",
  "Kopete is the new multi-protocol instant messenger");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot24.jpg",
  "images/3.2/snapshot24.png",
  160, 120,
  "A CD player is of course included",
  "",
  "A CD player is of course included");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot25.jpg",
  "images/3.2/snapshot25.png",
  160, 120,
  "CD-Ripper, music player and CD/DVD burner application (from KDE Extragear)",
  "",
  "CD-Ripper, music player and CD/DVD burner application (from KDE Extragear)");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.2/thumbs/snapshot26.jpg",
  "images/3.2/snapshot26.png",
  160, 120,
  "The media player Noatun has many different faces",
  "",
  "The media player Noatun has many different faces");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot28.jpg",
  "images/3.2/snapshot28.png",
  160, 120,
  "Accessibility and general utilities",
  "",
  "Accessibility and general utilities");

  $gallery->addImage(
  "images/3.2/thumbs/snapshot29.jpg",
  "images/3.2/snapshot29.png",
  160, 120,
  "KDE's sophisticated print system",
  "",
  "KDE's sophisticated print system");

  $gallery->show();
?>

<?php
  include "footer.inc"
?>
