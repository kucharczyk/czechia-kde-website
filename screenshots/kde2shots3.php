<?php
  $page_title = "KDE 2.0 Screenshots - Page 3";
  $site_root = "../";
  include_once ("header.inc");
?>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2">
<p>Here are some screenshots showing KDE 2.0 pre-alpha as of August 01 1999.
<i>Please be aware that the screenshots are a little bit outdated.
The current screenshots are those on the <a href="kde2shots.php">first page</a>.
</i><br />
Click on a preview screenshot for a full sized version.<br /><br />
Greetings, Matthias Elter :-)
</p>
<p>
<a href="kde2shots2.php">Previous page</a><br />
<a href="kde2shots4.php">Next page</a>
</p>
<hr />
</td>
</tr>

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
The first screenshot makes use of the Marble/Motif widget style.  You may notice the marble buttons, menubars, and toolbars.  KDE 2.0 features simple pixmap widget themes as well as powerful and very fast native widget themes based on widget drawing engines.  KDE 2.0 comes with a powerful theme designer app that makes creating new themes very easy. This screenshot shows a Konsole (notice the transparent background feature) and a Konqueror window.  Konqueror is KDE 2.0's new CORBA-based file manager and web browser.
</p>
<ul>
<li>
view <a href="images/large/kde2_1.jpg">1152&times;864</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2_1.jpg"><img src="images/mini/kde2_1.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
Here you can see KFind in the upper left, KPager in the lower left and again a Konqueror window.  You can split the Konqueror view horizontally or vertically in as many child views as needed.  Every child view can be configured to be a tree-view, icon-view, html-view or embed other CORBA aware applications like KView to display images.  Konqueror can be used to display _anything_.  It simply queries the KDE trader for a viewer service matching the mimetype of the file you want to display and embeds it.  Konqueror also supports a large variety of protocols, http, ftp, file, smb (windows network) and gopher to name a few.
</p>
<ul>
<li>
view <a href="images/large/kde2_2.jpg">1152&times;864</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2_2.jpg"><img src="images/mini/kde2_2.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
The third screenshot is using the pretty Gold/Gradient theme. It shows Kcmdisplay, the display control app in the upper left, KDiskNav (the panel's file system navigator) in the lower left, and KHelpcenter, KDE 2.0's new CORBA-based help browser on the right.  KHelpcenter is an OpenParts application like Konqueror and can embed all kinds of views to display, for example, html, man, info, ps, or dvi files.  KHelpcenter is one of my pet projects by the way.
</p>
<ul>
<li>
view <a href="images/large/kde2_3.jpg">1152&times;864</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2_3.jpg"><img src="images/mini/kde2_3.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
Ok, here is the last screenshot for today.  This one shows the use of the "System" theme and has the theme selector in the upper left, as well as two Konqueror windows.  The Konqy window in the lower left is used to browse my home dir while the large one on the right is used to browse <a href="http://www.kde.org">www.kde.org</a>. Lots of work has been done on the KDE HTMLWidget resulting in better performance and a smaller memory footprint. There is currently work going on adding DOM, JavaScript and Java support to Konqueror.
</p>
<ul>
<li>
view <a href="images/large/kde2_4.jpg">1152&times;864</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2_4.jpg"><img src="images/mini/kde2_4.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<tr>
<td colspan="2">
<hr />
<a href="kde2shots2.php">Previous page</a><br />
<a href="kde2shots4.php">Next page</a>
</td>
</tr>
</table>

<?php
  include_once ("footer.inc");
?>
