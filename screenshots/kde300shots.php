<?php
  $page_title = "KDE 3.0 Screenshots - Page 1";
  $site_root = "../";
  include_once ("header.inc");
?>

<p> Here are some screenshots showing <a
href="/announcements/announce-3.0.html">KDE 3.0</a>, an older version
of the powerful K Desktop Environment, a free desktop environment for
Unix.</p>

<p>
Click on a preview screenshot for a full sized version.</p>

<table border="0" cellspacing="0" cellpadding="3" width="100%">

<!-- ---------------- Navigation section ---------------------------- -->

<tr><td colspan="2">

<a href="kde300shots2.php">Next page</a>
<hr />

</td></tr>

<!-- ---------------- Screenshot section ---------------------------- -->

<tr valign="top">
<td width="270" valign="top">

<p>
How does KDE 3.0 look? However you want it to! You can choose from various
color themes, window manager decorations, widget styles and icon sets! Take
a look at the <a href="http://www.kde-look.org/">KDE-Look</a> website for
available customizations. KDE even supports <a
href="http://www.themes.org/themes/icewm/">IceWM themes</a>, as shown in
this screenshot which combines the new Slick icon style, the FreeKDE.org
color scheme and the Light widget style!</p>

<ul>
<li><a href="images/1152x864/kde300-snapshot1-1152x864.jpg">large version</a> (1152&times;864 .jpg, 139k)</li>
<li><a href="images/1152x864/kde300-snapshot1-1152x864.png">large version</a> (1152&times;864 .png, 347k)</li>
</ul>

</td><td width="320" valign="top">

<a href="images/1152x864/kde300-snapshot1-1152x864.jpg"><img
src="images/320x240/kde300-snapshot1-320x240.jpg" alt="KDE Desktop Environment"
width="320" height="240" border="0" /></a>

</td></tr>
<tr valign="top">
<td width="270" valign="top">

<p>
The default look of KDE with old-time favorite <a
href="http://konqueror.kde.org/">Konqueror</a>! The speed and quality of the
KHTML render component has been improved extensively and JavaScript support
has never been better before! For a complete list of changes and
improvements in KDE 3.0, check the <a
href="/announcements/changelog2_2_2to3_0.html">Changelog</a>.</p>

<ul>
<li><a href="images/1152x864/kde300-snapshot2-1152x864.jpg">large version</a> (1152&times;864 .jpg, 157k)</li>
<li><a href="images/1152x864/kde300-snapshot2-1152x864.png">large version</a> (1152&times;864 .png, 253k)</li>
</ul>

</td><td width="320" valign="top">

<a href="images/1152x864/kde300-snapshot2-1152x864.jpg"><img
src="images/320x240/kde300-snapshot2-320x240.jpg" alt="KDE Desktop Environment"
width="320" height="240" border="0" /></a>

</td></tr>
<tr valign="top">
<td width="270" valign="top">

<p>
KDE 3.0 offers you much more variety in how you want your desktop to look or
feel. Create the user interface that best suits your way of working and get
the most out of your favorite applications and features such as transparent
consoles.</p>

<ul>
<li><a href="images/1152x864/kde300-snapshot3-1152x864.jpg">large version</a> (1152&times;864 .jpg, 144k)</li>
<li><a href="images/1152x864/kde300-snapshot3-1152x864.png">large version</a> (1152&times;864 .png, 285k)</li>
</ul>

</td><td width="320" valign="top">

<a href="images/1152x864/kde300-snapshot3-1152x864.jpg"><img
src="images/320x240/kde300-snapshot3-320x240.jpg" alt="KDE Desktop Environment"
width="320" height="240" border="0" /></a>

</td></tr>

<!-- ---------------- Navigation section ---------------------------- -->

<tr><td colspan="2">

<hr />
<a href="kde300shots2.php">Next page</a>

</td></tr>

</table>

<?php
  include_once ("footer.inc");
?>
