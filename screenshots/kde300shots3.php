<?php
  $page_title = "KDE 3.0 Screenshots - Page 3";
  $site_root = "../";
  include_once ("header.inc");
?>

<p>Here are some screenshots showing KDE 3.0 CVS, the most recent
version of the powerful K Desktop Environment, a free desktop environment
for Unix.</p>
<!--There are packages (source and binary)
<A HREF="ftp://ftp.us.kde.org/pub/kde/unstable/distribution/current/">here</A>,
and more recent source snapshots
<A HREF="ftp://ftp.us.kde.org/pub/kde/snapshots/current/">here</A>.
Download them and get your own impression</b>!<BR>-->
<p>Click on a preview screenshot for a full sized version.</p>

<table border="0" cellspacing="0" cellpadding="0" width="100%">

<!-- ---------------- Screenshot section ---------------------------- -->
<tr><td colspan="2">

<a href="kde300shots2.php">Previous page</a>
<hr />

</td></tr>

<!-- screenshot start -->
<tr valign="top">
<td width="270" valign="top">
<p>
KDE 3.0 comes with your favourite applications such as Konqueror (shown in
file manager mode) and terminal program Konsole. One of the first styles
completely ported to the new Qt 3.0 style engine is the <a
  href="http://clee.azsites.org/kde/">dotNET widget style</a> by Chris
Lee.</p>
<ul>
<li>view <a href="images/large/kde3-snapshot1.jpg">large version</a> (1152&times;864, 224k)</li>
</ul>
</td><td width="320" valign="top">
<a href="images/large/kde3-snapshot1.jpg"><img src="images/mini/kde3-snapshot1-small.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td></tr>

<tr valign="top">
<td width="270" valign="top">
<p>
The default KDE 3.0 look. Opened are the traditional Control Center (with
the printer configuration module), cd-authoring and burning utility KonCD
and the friendly tips from Kandalf.</p>
<ul>
<li>view <a href="images/large/kde3-snapshot2.jpg">large</a> version (1152&times;864, 172k)</li>
</ul>
</td><td width="320" valign="top">
<a href="images/large/kde3-snapshot2.jpg"><img src="images/mini/kde3-snapshot2-small.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td></tr>

<tr valign="top">
<td width="270" valign="top">
<p>
Rik Hemsley's web engine window manager decoration together with the iKons
iconset, one of the various visual improvements for KDE available from <a
  href="http://www.kde-look.org/">www.kde-look.org</a>. <a
  href="http://konqueror.kde.org/">Konqueror</a> in web browser made continues
to be the pride of the <a href="http://developer.kde.org/">KDE
  development</a> team.</p>
<ul>
<li>view <a href="images/large/kde3-snapshot3.jpg">large</a> version (1152&times;864, 221k)</li>
</ul>
</td><td width="320" valign="top">
<a href="images/large/kde3-snapshot3.jpg"><img src="images/mini/kde3-snapshot3-small.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td></tr>

<tr valign="top">
<td width="270" valign="top">
<p>

KDE is not all work, but also some play! The Media Peach colour scheme and
  Quartz window decorations give KDE a colourful appearance. <a
  href="http://capsi.com/atlantik/">Atlantik</a> is one of the many <a
  href="http://games.kde.org/">games</a> available for KDE. Also shown is
  the audiocd KIO slave which allows you to access data on audio compact
  discs.  Updating your mp3 or ogg audio archive is as easy as drag and
  drop! Many other KIO slaves are available, such as the SFTP KIO slave
  which allows you to securely copy files over a network.</p>
<ul>
<li>view <a href="images/large/kde3-snapshot4.jpg">large</a> version (1152&times;864, 229k)</li>
</ul>
</td><td width="320" valign="top">
<a href="images/large/kde3-snapshot4.jpg"><img src="images/mini/kde3-snapshot4-small.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td></tr>

<tr><td colspan="2">
<a href="kde300shots2.php">Previous page</a>
</td></tr>

</table>

<?php
  include_once ("footer.inc");
?>
