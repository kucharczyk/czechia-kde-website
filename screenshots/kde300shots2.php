<?php
  $page_title = "KDE 3.0 Screenshots - Page 2";
  $site_root = "../";
  include_once ("header.inc");
?>

<p>
Here are some screenshots showing KDE 3.0 Beta2, the most recent version of
the powerful K Desktop Environment, a free desktop environment for Unix.</p>

<p>
Click on a preview screenshot for a full sized version.</p>

<table border="0" cellspacing="0" cellpadding="3" width="100%">

<!-- ---------------- Navigation section ---------------------------- -->

<tr><td colspan="2">

<a href="kde300shots.php">Previous page</a>
<br /><a href="kde300shots3.php">Next page</a>
<hr />

</td></tr>

<!-- ---------------- Screenshot section ---------------------------- -->

<tr valign="top">
<td width="270" valign="top">

<p>
How does KDE 3.0 look? However you want it to! You can choose from various
color themes, window manager decorations, widget styles and icon sets! Take
a look at the <a href="http://www.kde-look.org/">KDE-Look</a> website for
available customizations. KDE even supports <a
href="http://www.themes.org/themes/icewm/">IceWM themes</a>, as shown in
this screenshot!</p>

<ul>
<li><a href="images/1152x864/kde300beta2-snapshot1-1152x864.jpg">large version</a> (1152&times;864, 192k)</li>
</ul>

</td><td width="320" valign="top">

<a href="images/1152x864/kde300beta2-snapshot1-1152x864.jpg"><img
src="images/320x240/kde300beta2-snapshot1-320x240.jpg" alt="KDE Desktop Environment"
width="320" height="240" border="0" /></a>

</td></tr>
<tr valign="top">
<td width="270" valign="top">

<p>
Another look for KDE, including some applications such as one of the text
editors that offer syntax high-lighting and <a
href="http://www.freekde.org/neil/megami/">Megami</a>, a Blackjack game for
KDE. Also shown is the new file dialog.</p>

<ul>
<li><a href="images/1152x864/kde300beta2-snapshot2-1152x864.jpg">large version</a> (1152&times;864, 145k)</li>
</ul>

</td><td width="320" valign="top">

<a href="images/1152x864/kde300beta2-snapshot2-1152x864.jpg"><img
src="images/320x240/kde300beta2-snapshot2-320x240.jpg" alt="KDE Desktop Environment"
width="320" height="240" border="0" /></a>

</td></tr>
<tr valign="top">
<td width="270" valign="top">

<p>
If you like media players, transparent Konsoles, games (both <a
href="http://edu.kde.org/">educational</a> as <a
href="http://games.kde.org/">pure entertainment</a>) or even the formation
of the stars, KDE has the right applications for you!</p>

<ul>
<li><a href="images/1152x864/kde300beta2-snapshot3-1152x864.jpg">large version</a> (1152&times;864, 154k)</li>
</ul>

</td><td width="320" valign="top">

<a href="images/1152x864/kde300beta2-snapshot3-1152x864.jpg"><img
src="images/320x240/kde300beta2-snapshot3-320x240.jpg" alt="KDE Desktop Environment"
width="320" height="240" border="0" /></a>

</td></tr>
<tr valign="top">
<td width="270" valign="top">

<p>
KDE allows you to place the panel and taskbar almost any way you like and
also hides it for you if you really need all that screen estate for other
applications, such as <a href="http://noatun.kde.org/">Noatun</a>, the KDE
media player. Noatun comes with several <a
href="http://noatun.kde.org/plugins.phtml">plugins</a> to enhance your
listening or viewing experience.</p>

<ul>
<li><a href="images/1152x864/kde300beta2-snapshot4-1152x864.jpg">large version</a> (1152&times;864, 182k)</li>
</ul>

</td><td width="320" valign="top">

<a href="images/1152x864/kde300beta2-snapshot4-1152x864.jpg"><img
src="images/320x240/kde300beta2-snapshot4-320x240.jpg" alt="KDE Desktop Environment"
width="320" height="240" border="0" /></a>

</td></tr>

<!-- ---------------- Navigation section ---------------------------- -->

<tr><td colspan="2">

<hr />
<a href="kde300shots.php">Previous page</a>
<br /><a href="kde300shots3.php">Next page</a>

</td></tr>

</table>

<?php
  include_once ("footer.inc");
?>
