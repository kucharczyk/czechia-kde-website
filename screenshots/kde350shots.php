<?php
  $page_title = "KDE 3.5 series Screenshots";
  $site_root = "../";
  include "header.inc";
?>

<p> Here are some screenshots showing the powerful K&nbsp;Desktop&nbsp;Environment version 3.5, a free desktop environment for Unix. <a
href="../announcements/announce-3.5.4.php">(KDE 3.5.4 announcement)</a></p>

<p> The world of KDE has expanded far beyond the "official base" set. It now includes office, creativity, communication and many other free and commercial applications. A goal of this screen shot series is to show how closely one can attune KDE by adding only a few of the many of add-on software and customizations available for KDE.</p>

<p> This collection is trying to tell a story of KDE from a user's point of view. Hence, the featured desktop was expanded to include some freely available, KDE-based applications that do not come with "official base" KDE set. Visit <a href="../family/">KDE Family page</a> to find other exciting KDE applications and places.</p>

<p>Don't miss <a href="http://www.kde.org/announcements/visualguide-3.5.php">KDE 3.5: A Visual Guide to New Features</a></p>

<p>All these screenshots are available in a <a 
href="http://spread.kde.org/files/kde35screenies.pdf">"ready-for-presentation" PDF file</a>.</p>

<?php
  $gallery = new ImageGallery("These are screenshots of KDE 3.5.x");

  $gallery->addImage(
  "images/3.5/thumbs/01-welcome.png",
  "images/3.5/01-welcome.png",
  160, 120,
  "",
  "",
  "Window transparencies with X composite extension.");

  $gallery->addImage(
  "images/3.5/thumbs/02-systeminfo.png",
  "images/3.5/02-systeminfo.png",
  160, 120,
  "",
  "",
  "KDE runs smoothly even on slow computers");

  $gallery->addImage(
  "images/3.5/thumbs/03-kicker.png",
  "images/3.5/03-kicker.png",
  160, 120,
  "",
  "",
  "<strong>Kicker</strong>: our taskbar");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/04-konqueror-multipurpose.png",
  "images/3.5/04-konqueror-multipurpose.png",
  160, 120,
  "",
  "",
  "Is it a web browser? Is it a file brower? Yes! But, we just call it <strong>Konqueror</strong>.");

  $gallery->addImage(
  "images/3.5/thumbs/05-konqueror-kio.png",
  "images/3.5/05-konqueror-kio.png",
  160, 120,
  "",
  "",
  "<strong>KIO Plugins</strong> - a bunch of magic potions for <strong>Konqueror</strong>'s location entry field.");

  $gallery->addImage(
  "images/3.5/thumbs/06-konqueror-mc.png",
  "images/3.5/06-konqueror-mc.png",
  160, 120,
  "",
  "",
  "<strong>Konqueror</strong>? Wearing Midnight Commander - like dress?");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/07-konqueror-photos.png",
  "images/3.5/07-konqueror-photos.png",
  160, 120,
  "",
  "",
  "Which of the image preview methods appeals to you most?");

  $gallery->addImage(
  "images/3.5/thumbs/08-konqueror-video.png",
  "images/3.5/08-konqueror-video.png",
  160, 120,
  "",
  "",
  "<strong>Konqueror</strong> has thumbnails and preview for video too.");

  $gallery->addImage(
  "images/3.5/thumbs/09-kmail.png",
  "images/3.5/09-kmail.png",
  160, 120,
  "",
  "",
  "<strong>Kmail</strong> - the \"little\" email client that can.");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/10-kaddressbook.png",
  "images/3.5/10-kaddressbook.png",
  160, 120,
  "",
  "",
  "<strong>Kaddressbook</strong>, the KDE's contact management application.");

  $gallery->addImage(
  "images/3.5/thumbs/11-kopete.png",
  "images/3.5/11-kopete.png",
  160, 120,
  "",
  "",
  "You can use your MSN Messenger account (and many more) with <strong>Kopete</strong>");

  $gallery->addImage(
  "images/3.5/thumbs/12-korganizer.png",
  "images/3.5/12-korganizer.png",
  160, 120,
  "",
  "",
  "<strong>Korganizer</strong> - KDE's scheduling and task management application.");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/13-kalarm.png",
  "images/3.5/13-kalarm.png",
  160, 120,
  "",
  "",
  "<strong>Kalarm</strong> a simple solution for complex time-keeping needs.");

  $gallery->addImage(
  "images/3.5/thumbs/14-akregator.png",
  "images/3.5/14-akregator.png",
  160, 120,
  "",
  "",
  "<strong>aKregator</strong> - taking \"managing\" out of \"reading\" RSS (Blog) feeds.");

  $gallery->addImage(
  "images/3.5/thumbs/15-internet-transferassistants.png",
  "images/3.5/15-internet-transferassistants.png",
  160, 120,
  "",
  "",
  "<strong>Ktorrent</strong> and <strong>KGet</strong> make downloading stuff from the Internet easy.");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/16-remotedesktop.png",
  "images/3.5/16-remotedesktop.png",
  160, 120,
  "",
  "",
  "With <strong>KDE Desktop Sharing</strong> you can receive help from and give help to your remote friend.");

  $gallery->addImage(
  "images/3.5/thumbs/17-voip-twinkle.png",
  "images/3.5/17-voip-twinkle.png",
  160, 120,
  "",
  "",
  "Wondered if KDE is ready for VoIP? The answer is a definite \"Yes.\"");

  $gallery->addImage(
  "images/3.5/thumbs/18-terminal.png",
  "images/3.5/18-terminal.png",
  160, 120,
  "",
  "",
  "Still feel the urge to punch commands in a terminal? KDE offers <strong>Konsole</strong> and <strong>Yakuake</strong>");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/19-plug-n-play.png",
  "images/3.5/19-plug-n-play.png",
  160, 120,
  "",
  "",
  "Here is what happens when music player, camera, or USB memory stick is plugged into the computer.");

  $gallery->addImage(
  "images/3.5/thumbs/20-plug-n-play-digikam.png",
  "images/3.5/20-plug-n-play-digikam.png",
  160, 120,
  "",
  "",
  "Getting your pictures out of your camera became easy with <strong>Digikam</strong>.");

  $gallery->addImage(
  "images/3.5/thumbs/21-digiKam.png",
  "images/3.5/21-digiKam.png",
  160, 120,
  "",
  "",
  "<strong>Digikam</strong> is a very powerful photo management application.");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/22-open-save-window.png",
  "images/3.5/22-open-save-window.png",
  160, 120,
  "",
  "",
  "<strong>KDE</strong> keeps you in control of your media all the way from Open, to view/edit, to Save windows.");

  $gallery->addImage(
  "images/3.5/thumbs/23-printing.png",
  "images/3.5/23-printing.png",
  160, 120,
  "",
  "",
  "The <strong>KDE print</strong> system - You are not the only one who didn't expect it to be so advanced.");

  $gallery->addImage(
  "images/3.5/thumbs/24-kpdf.png",
  "images/3.5/24-kpdf.png",
  160, 120,
  "",
  "",
  "Faster than a speeding electron, it is <strong>Kpdf</strong> - KDE's pdf viewer, digger, presentation conjurer.");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/25-cdvd-burning.png",
  "images/3.5/25-cdvd-burning.png",
  160, 120,
  "",
  "",
  "<strong>K3B</strong> - application that defines the \"leading edge\" for CD and DVD burning on the open source desktop.");

  $gallery->addImage(
  "images/3.5/thumbs/26-games.png",
  "images/3.5/26-games.png",
  160, 120,
  "",
  "",
  "KDE strives to make you productive in everything you do, even if that thing is \"to be UNproductive\"");

  $gallery->addImage(
  "images/3.5/thumbs/27-juk.png",
  "images/3.5/27-juk.png",
  160, 120,
  "",
  "",
  "<strong>Juk</strong> - Do you know a simpler way to manage and listen to your music files collection?.");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/28-amarok.png",
  "images/3.5/28-amarok.png",
  160, 120,
  "",
  "",
  "If you want a full featured audio player, <strong>Amarok</strong> is all you need! ");

  $gallery->addImage(
  "images/3.5/thumbs/29-kaffeine.png",
  "images/3.5/29-kaffeine.png",
  160, 120,
  "",
  "",
  "<strong>Kaffeine</strong> - the \"yeah! I can play that too,\" slick and powerful media player.");

  $gallery->addImage(
  "images/3.5/thumbs/30-programming.png",
  "images/3.5/30-programming.png",
  160, 120,
  "",
  "",
  "Create your visual application without any knowledge of a programming language with <strong>Kommander</strong>");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/31-texteditors.png",
  "images/3.5/31-texteditors.png",
  160, 120,
  "",
  "",
  "Time to put vim to pasture? Meet the not so simple text editors <strong>Kate</strong> and <strong>KWrite</strong>.");

  $gallery->addImage(
  "images/3.5/thumbs/32-htmleditor.png",
  "images/3.5/32-htmleditor.png",
  160, 120,
  "",
  "",
  "<strong>Quanta Plus</strong> - \"What You See Is What You Get\" web editor. ");

  $gallery->addImage(
  "images/3.5/thumbs/33-controlcenter.png",
  "images/3.5/33-controlcenter.png",
  160, 120,
  "",
  "",
  "Customize and configure your KDE system to fit your desires with <strong>KDE's Control Center</strong>");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.5/thumbs/34-languages.png",
  "images/3.5/34-languages.png",
  160, 120,
  "",
  "",
  "KDE 3.5 is available in <strong>63 <br>languages</strong>.");

  $gallery->addImage(
  "images/3.5/thumbs/35-superkaramba.png",
  "images/3.5/35-superkaramba.png",
  160, 120,
  "",
  "",
  "Beware: With <strong>SuperKaramba</strong>, even the strongest will fall to desktop applet addiction.");

  $gallery->addImage(
  "images/3.5/thumbs/36-screenshots.png",
  "images/3.5/36-screenshots.png",
  160, 120,
  "",
  "",
  "KDE can be as austere or as eye-candy as you want.");

  $gallery->show();
?>


<p>Please, visit <a href="http://wiki.kde.org/tiki-index.php?page=Screenshots">Screenshots 
Section of KDE Wiki</a> to view our extensive screen shot collection.</p>

<?php
  include "footer.inc"
?>