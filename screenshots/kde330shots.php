<?php
  $page_title = "KDE 3.3 Screenshots";
  $site_root = "../";
  include "header.inc";
?>

<p> Here are some screenshots showing <a
href="../announcements/announce-3.3.php">KDE 3.3</a>, an older version
of the powerful K&nbsp;Desktop&nbsp;Environment, a free desktop environment for
Unix.</p>

<?php
  $gallery = new ImageGallery("These are screenshots of KDE 3.3.x");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot1.jpg",
  "images/3.3/snapshot1.png",
  160, 120,
  "KDE splash screen",
  "",
  "KDE splash screen");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot2.jpg",
  "images/3.3/snapshot2.png",
  160, 120,
  "The desktop settings wizard offers Plastik if installed",
  "",
  "The desktop settings wizard offers Plastik if installed");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot3.jpg",
  "images/3.3/snapshot3.png",
  160, 120,
  "Kolourpaint is a newly included paint program",
  "",
  "Kolourpaint is a newly included paint program");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.3/thumbs/snapshot4.jpg",
  "images/3.3/snapshot4.png",
  160, 120,
  "A Logo turtle, a word quiz and a latin trainer joined KDE Edutainment",
  "",
  "A Logo turtle, a word quiz and a latin trainer joined KDE Edutainment");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot5.jpg",
  "images/3.3/snapshot5.png",
  160, 120,
  "The new theme manager",
  "",
  "The new theme manager");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot6.jpg",
  "images/3.3/snapshot6.png",
  160, 120,
  "A link checker and an image map editor joined the web development module",
  "",
  "A link checker and an image map editor joined the web development module");

  $gallery->startNewRow();

    $gallery->addImage(
  "images/3.3/thumbs/snapshot7.jpg",
  "images/3.3/snapshot7.png",
  160, 120,
  "The new look of the window switch popup",
  "",
  "The new look of the window switch popup");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot8.jpg",
  "images/3.3/snapshot8.png",
  160, 120,
  "The browser with search bar plugin, newsticker sidebar and type-ahead find",
  "",
  "The browser with search bar plugin, newsticker sidebar and type-ahead find");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot9.jpg",
  "images/3.3/snapshot9.png",
  160, 120,
  "KMail can now compose HTML messages",
  "",
  "KMail can now compose HTML messages");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.3/thumbs/snapshot10.jpg",
  "images/3.3/snapshot10.png",
  160, 120,
  "Anti-spam and anti-virus wizards help with the setup",
  "",
  "Anti-spam and anti-virus wizards help with the setup");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot11.jpg",
  "images/3.3/snapshot11.png",
  160, 120,
  "The summary view of the PIM suite Kontact",
  "",
  "The summary view of the PIM suite Kontact");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot12.jpg",
  "images/3.3/snapshot12.png",
  160, 120,
  "KOrganizer working as part within Kontact",
  "",
  "KOrganizer working as part within Kontact");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.3/thumbs/snapshot13.jpg",
  "images/3.3/snapshot13.png",
  160, 120,
  "The instant messenger integration with the addressbook and the file browser",
  "",
  "The instant messenger integration with the addressbook and the file browser");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot14.jpg",
  "images/3.3/snapshot14.png",
  160, 120,
  "KDE ships an Integrated Development Environment",
  "",
  "KDE ships an Integrated Development Environment");

  $gallery->addImage(
  "images/3.3/thumbs/snapshot15.jpg",
  "images/3.3/snapshot15.png",
  160, 120,
  "And also with a web development IDE",
  "",
  "And also with a web development IDE");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.3/thumbs/snapshot16.jpg",
  "images/3.3/snapshot16.png",
  160, 120,
  "Konqueror with the document relations plugin and lookup of selected text",
  "",
  "Konqueror with the document relations plugin and lookup of selected text");

  $gallery->show();
?>

<?php
  include "footer.inc"
?>
