<?php
  $page_title = "KDE 1.x Screenshots - Page 3";
  $site_root = "../";
  include_once ("header.inc");
?>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2">
<p>
Click on a preview screenshot for an 800&times;600 Pixel version. Below the descriptions, you will find a link to a full size version. You can also find a number of interesting screenshots and KDE configurations at Themes.org's 
<!--Commented out. Outdated. <a href="http://kde.themes.org/">-->
KDE Themes Pages.
<!--</a>-->
<em>Current KDE Themes can be found on <a href="http://www.kde-look.org">KDE Look</a></em>.
</p>
<p>
<a href="kde1shots4.php">Next Page</a><br />
<a href="kde1shots2.php">Previous Page</a>
</p>
<hr />
</td>
</tr>

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This is a shot of David Faure's desktop showing two windows of kfm, the user-friendly file manager and web browser (customized icons in the first, directory tree and detailed view in the second), reversi game, CD player, and a graphical talk request announce.
</p>
<ul>
<li>
view <a href="images/medium/dfaure.jpg">800&times;600</a> version
</li>
<li>
view <a href="images/large/dfaure.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/dfaure.jpg"><img src="images/mini/dfaure.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td></tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This screenshots shows a dark green theme. The panel is located on the right and the taskbar on the bottom of the desktop in this configuration. The MacOS-like global menu bar is activated as well. You can see a knote window in the upper right and a kfm session used as a web browser in the foreground.
</p>
<ul>
<li>
view <a href="images/medium/sks1.jpg">800&times;600</a> version
</li>
<li>
view <a href="images/large/sks1.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/sks1.jpg"><img src="images/mini/sks1.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td></tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This is a shot of Steffen Hansen's desktop. You may notice the MacOS-like window decorations. Changing the look and feel of your KDE desktop is very easy and you don't have to edit configuration files by hand to do so. You can see the KDE control center in the upper left, a file manager session and the KDE task manager on this shot.
</p>
<ul>
<li>
view <a href="images/medium/hansen1.jpg">800&times;600</a> version
</li>
<li>
view <a href="images/large/hansen1.jpg">1152&times;864</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/hansen1.jpg"><img src="images/mini/hansen1.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
You can see a nice dark theme on this screenshot. KDE supports all kinds of themes. This screenshot shows a gimp and a quake session. You can see how nicely non-KDE apps like the gimp integrate into the desktop. On the lower right you can see kmixer, the KDE sound mixer.
</p>
<ul>
<li>
view <a href="images/medium/darktheme.jpg">800&times;640</a> version
</li>
<li>
view <a href="images/large/darktheme.jpg">1280&times;1024</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/darktheme.jpg"><img src="images/mini/darktheme.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<tr>
<td colspan="2">
<hr />
<a href="kde1shots4.php">Next Page</a><br />
<a href="kde1shots2.php">Previous Page</a>
</td>
</tr>
</table>

<?php
  include_once ("footer.inc");
?>
