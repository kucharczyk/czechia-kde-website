<?php
  $page_title = "KDE 1.x Screenshots - Page 1";
  $site_root = "../";
  include_once ("header.inc");
?>

<table border="0" cellspacing="0" cellpadding="0" >
<tr>
<td colspan="2">
<p>
Click on a preview screenshot for an 800&times;600 Pixel version. Below the descriptions, you will find a link to a full size version. You can also find a number of interesting screenshots and KDE configurations at Themes.org's 
<!--Commented out. Outdated. <a href="http://kde.themes.org/">-->
KDE Themes Pages.
<!--</a>-->
<em>Current KDE Themes can be found on <a href="http://www.kde-look.org">KDE Look</a></em>.
</p>
<p>
<a href="kde1shots2.php">Next Page</a>
</p>
<hr />
</td>
</tr>

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This is a screenshot of my desktop showing the new KDE 1.1 desktop pager in the upper right, a file manager session, a konsole session and the disk navigator docked in the panel. Konsole is a new virtual console, a replacement for kvt. The disk navigator is an easy way to browse through your local files.
</p>
<ul>
<li>
view <a href="images/medium/matthiase1.jpg">800&times;600</a> version
</li>
<li>
view <a href="images/large/matthiase1.jpg">1280&times;960</a> version</li>
</ul>
</td>
<td>
<a href="images/medium/matthiase1.jpg"><img src="images/mini/matthiase1.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This is a shot of <a href="&#109;&#x61;i&#108;&#116;o:&#x67;&#114;&#097;&#110;&#114;&#x6f;&#116;&#x68;&#64;k&#100;e&#x2e;o&#x72;&#103;">Kurt Granroth's</a> desktop. This screenshot proves that KDE does not have to look like Windows or like a Macintosh, it can look like whatever you want it to. You can see a kfm session used to browse Kurt's homepage, the configuration dialog of Kurt's pet project kbiff, kscd and a konsole session on this nice shot.  This theme can be found on kde.themes.org <!--<a href="http://kde.themes.org/php/download.phtml?object=kde.theme.935281335&amp;rev=1.1.2">-->here <em>(link outdated)</em></a>.
</p>
<ul>
<li>
view <a href="images/medium/ganroth.jpg">800&times;640</a> version
</li>
<li>
view <a href="images/large/ganroth.jpg">1280&times;1024</a> version
</li>
</ul>
</td>
<td width="320">
<a href="images/medium/ganroth.jpg"><img src="images/mini/ganroth.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
While we suggest that you use kwm, the KDE window manager, which offers additional features for advanced integration with other KDE applications, you can use any other X11R6 window manager with KDE. This screenshot shows KDE running with the WindowMaker window manager. While kwm still offers the best integration with other KDE apps, window managers like WindowMaker or BlackBox today also come with advanced KDE support.
</p>
<ul>
<li>
view <a href="images/medium/wmaker1.jpg">800&times;640</a> version
</li>
<li>
view <a href="images/large/wmaker1.jpg">1280&times;1024</a> version
</li>
</ul>
</td>
<td width="320">
<a href="images/medium/wmaker1.jpg"><img src="images/mini/wmaker1.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This and the next screenshot were donated by <a href="mailto:a11576@aaual.ualg.pt">Leonel Martins</a>. I especially liked the icons.
</p>
<ul>
<li>
view <a href="images/medium/pollux-1.jpg">904&times;675</a> version
</li>
</ul>
</td>
<td width="320">
<a href="images/medium/pollux-1.jpg"><img src="images/mini/pollux-1.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
Another screenshot from <a href="mailto:a11576@aaual.ualg.pt">Leonel Martins</a>.
</p>
<ul>
<li>
view <a href="images/medium/pollux-2.jpg">904&times;675</a> version
</li>
</ul>
</td>
<td width="320">
<a href="images/medium/pollux-2.jpg"><img src="images/mini/pollux-2.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<tr style="vertical-align:top;">
<td colspan="2">
<hr />
<a href="kde1shots2.php">Next Page</a>
</td>
</tr>
</table>

<?php
  include_once ("footer.inc");
?>
