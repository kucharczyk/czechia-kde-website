<?php
  $page_title = "KDE 1.x Screenshots - Page 2";
  $site_root = "../";
  include_once ("header.inc");
?>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2">
<p>
Click on a preview screenshot for an 800&times;600 Pixel version. Below the descriptions, you will find a link to a full size version. You can also find a number of interesting screenshots and KDE configurations at Themes.org's 
<!--Commented out. Outdated. <a href="http://kde.themes.org/">-->
KDE Themes Pages
<!--</a>-->
<em>Current KDE Themes can be found on <a href="http://www.kde-look.org">KDE Look</a></em>.
</p>
<p>
<a href="kde1shots3.php">Next Page</a><br />
<a href="kde1shots.php">Previous Page</a>
</p>
<hr />
</td>
</tr>

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
A team of KDE developers is currently working on KOffice, the CORBA based KDE productivity suite. KOffice comes with a word processor (KWord), a spread sheet (KSpread), a vector drawing program (KIllustrator), a presentation program (KPresenter) and small tools like a formula editor. This shot shows KWord, KIllustrator and KDiagram. KDE developers are already using KOffice for their daily work. Monitor <a href="http://koffice.org/">koffice.org</a> for a public beta soon to be released!
</p>
<ul>
<li>
view <a href="images/medium/koffice1.jpg">800&times;640</a> version
</li>
<li>
view <a href="images/large/koffice1.jpg">1280&times;1024</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/koffice1.jpg"><img src="images/mini/koffice1.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This screenshot shows KLyX, the KDE incarnation of the popular word processor LyX. LyX combines the WYSIWYG concept of modern word processors with the unsurpassed print quality of TeX. KLyX differs very much from common word processors because you tell KLyX what you are doing and KLyX deals with the rest. You don't concern yourself with what character goes where. KLyX integrates the power of LyX into your favorite desktop environment.
</p>
<ul>
<li>
view <a href="images/medium/klyx1.jpg">800&times;640</a> version
</li>
<li>
view <a href="images/large/klyx1.jpg">1280&times;1024</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/klyx1.jpg"><img src="images/mini/klyx1.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This screenshot shows my desktop while I'm working on the KDE screenshot pages. It shows xemacs to edit and a kfm session to display a screenshot page. I have activated MacOS-style menubars for this shot and you may notice that I configured the taskbar to be on the upper left corner of the desktop. In the lower left you can see the K-menu used as a fast and easy way to launch applications.
</p>
<ul>
<li>
view <a href="images/medium/matthiase2.jpg">800&times;600</a> version
</li>
<li>
view <a href="images/large/matthiase2.jpg">1280&times;960</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/matthiase2.jpg"><img src="images/mini/matthiase2.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This screenshot shows KDE running with the BlackBox window manager. BlackBox offers KDE support since version 0.5. You can see aktion, the KDE xanim frontend in the lower right and an early developer release of KIllustrator, the KOffice vector drawing application, in the center of the screen.
</p>
<ul>
<li>
view <a href="images/medium/blackbox1.jpg">800&times;600</a> version
</li>
<li>
view <a href="images/large/blackbox1.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/blackbox1.jpg"><img src="images/mini/blackbox1.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This is a second shot showing KDE and WindowMaker. You may notice the kwrite window in the upper left. kwrite is an advanced editor with syntax highlighting support that comes with KDE 1.1. You can also see a kfm session used as filemanager in the upper right and a kfm session used as web browser in the lower right.
</p>
<ul>
<li>
view <a href="images/medium/wmaker2.jpg">800&times;640</a> version
</li>
<li>
view <a href="images/large/wmaker2.jpg">1280&times;1024</a> version
</li>
</ul>
</td>
<td>
<a href="images/medium/wmaker2.jpg"><img src="images/mini/wmaker2.gif" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<tr>
<td colspan="2">
<hr />
<a href="kde1shots3.php">Next Page</a><br />
<a href="kde1shots.php">Previous Page</a>
</td>
</tr>
</table>

<?php
  include_once ("footer.inc");
?>
