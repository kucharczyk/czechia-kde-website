<?php
  $page_title = "KDE 2.0 Screenshots - Page 1";
  $site_root = "../";
  include_once ("header.inc");
?>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2">
<p>
Here are some screenshots showing KDE 2.0 final<br />
Click on a preview screenshot for a full sized version.
</p>
<p>
<a href="kde2shots2.php">Next page</a>
</p>
<hr />
</td>
</tr>

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
Welcome to KDE!
</p>
<p>
Load <a href="/info/2.0.html">KDE 2.0 final</a> and this should be the exact
screen you'll get. Learn new tricks from Kandalf, or dive straight into the
KDE panel called kicker, where you can launch applications and configure
your desktop.
</p>
<ul>
<li>
view <a href="images/large/kde2final_1.jpg">1024&times;768</a> version</li>
</ul>
</td>
<td>
<a href="images/large/kde2final_1.jpg"><img src="images/mini/kde2final_1.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
Not satisfied with the default look of KDE? No problem! You can easily
configure colors, widget and window decorations. This screenshot shows
<a href="http://konqueror.kde.org/">Konqueror</a> in action, browsing the FTP
archives and downloading one of the internationalization packages of KDE.
At the moment of release there were 15 translations available for KDE,
20 more to come in the next release!
</p>
<ul>
<li>
view <a href="images/large/kde2final_2.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2final_2.jpg"><img src="images/mini/kde2final_2.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
Yet another combination of colors and styles shows the improved Control
Center, which allows you to change many settings to your own likings. It is
also a good overview of some of the advanced possibilities of KDE, such as
the support for all Netscape plug-ins within the Konqueror browser. As you
can see from the ICQ registration window many third party applications are
already available for KDE2. An excellent guide of these can be found at
<a href="http://kde-apps.org/">kde-apps.org</a>.
</p>
<ul>
<li>
view <a href="images/large/kde2final_3.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2final_3.jpg"><img src="images/mini/kde2final_3.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
Some people say KDE is too much of a Windows clone.
This is not particularly true, as other screenshots have shown: KDE
attempts to take the best parts of various existing desktops such as MacOS,
CDE, BeOS, NeXt and indeed, also Windows. Furthermore the K Desktop
Environment enhances your desktop experience with its own improvements,
and the look is fully customizable, to suit your own taste.
</p>
<p>
This screenshot however <em>does</em> have a great Windows look and
feel, merely to demonstrate that KDE can fulfill the needs of users
new to UNIX who are adapting to a new environment. Our goal has always
been to deliver UNIX to the desktop and accommodating these needs is
one of the requirements to reach that goal.
</p>
<ul>
<li>
view <a href="images/large/kde2final_4.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2final_4.jpg"><img src="images/mini/kde2final_4.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<tr>
<td colspan="2">
<hr />
<a href="kde2shots2.php">Next page</a>
</td>
</tr>
</table>

<?php
  include_once ("footer.inc");
?>
