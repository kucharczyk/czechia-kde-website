<?php
  $page_title = "Náhledy KDE 3.1";
  $site_root = "../";
  include "header.inc";
?>

<p> Here are some screenshots showing <a
href="../announcements/announce-3.1.php">KDE 3.1</a>, an older version
of the powerful K&nbsp;Desktop&nbsp;Environment, a free desktop environment for
Unix.</p>

<?php
  $gallery = new ImageGallery("These are screenshots of KDE 3.1.x");

  $gallery->addImage(
  "images/3.1/thumbs/0.jpg",
  "images/3.1/fullsize/0.png",
  160, 120,
  "KDE Splash screen",
  "",
  "KDE Splash screen");

  $gallery->addImage(
  "images/3.1/thumbs/1.jpg",
  "images/3.1/fullsize/1.png",
  160, 120,
  "Webový prohlížeč KDE s podporou prohlížení v panelech a rozšířenou postranní lištou",
  "",
  "Webový prohlížeč KDE s podporou prohlížení v panelech a rozšířenou postranní lištou");

  $gallery->addImage(
  "images/3.1/thumbs/11.jpg",
  "images/3.1/fullsize/11.png",
  160, 120,
  "Podpora S/MIME v KMailu",
  "",
  "Podpora S/MIME v KMailu");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.1/thumbs/2.jpg",
  "images/3.1/fullsize/2.png",
  160, 120,
  "Editor Kate s podporou zvýraznění syntaxe a integrovaný správce souborů",
  "",
  "Editor Kate s podporou zvýraznění syntaxe a integrovaný správce souborů");

  $gallery->addImage(
  "images/3.1/thumbs/3.jpg",
  "images/3.1/fullsize/3.png",
  160, 120,
  "Přizpůsobení vzhledu KDE v panelu stylů a barev",
  "",
  "Přizpůsobení vzhledu KDE v panelu stylů a barev");

  $gallery->addImage(
  "images/3.1/thumbs/4.jpg",
  "images/3.1/fullsize/4.png",
  160, 120,
  "Výchozí nastavení vzhledu v KDE 3.1 v málo barvách",
  "",
  "Výchozí nastavení vzhledu v KDE 3.1 v málo barvách");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.1/thumbs/5.jpg",
  "images/3.1/fullsize/5.png",
  160, 120,
  "Vylepšená kontextová nápověda a integrovaný správce stahování",
  "",
  "Vylepšená kontextová nápověda a integrovaný správce stahování");

  $gallery->addImage(
  "images/3.1/thumbs/6.jpg",
  "images/3.1/fullsize/6.png",
  160, 120,
  "Zjednodušený panel nastavení s náhledem v reálném čase",
  "",
  "Zjednodušený panel nastavení s náhledem v reálném čase");

  $gallery->addImage(
  "images/3.1/thumbs/7.jpg",
  "images/3.1/fullsize/7.png",
  160, 120,
  "Nové hry a vylepšený klient IRC",
  "",
  "Nové hry a vylepšený klient IRC");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.1/thumbs/8.jpg",
  "images/3.1/fullsize/8.png",
  160, 120,
  "Vyberte si vzhled, který preferujete, v KPersonalizeru",
  "",
  "Vyberte si vzhled, který preferujete, v KPersonalizeru");

  $gallery->addImage(
  "images/3.1/thumbs/9.jpg",
  "images/3.1/fullsize/9.png",
  160, 120,
  "Správce tisku je připraven pro nejširší nasazení",
  "",
  "Správce tisku je připraven pro nejširší nasazení");

  $gallery->addImage(
  "images/3.1/thumbs/91.jpg",
  "images/3.1/fullsize/91.png",
  160, 120,
  "Konfigurace tiskáren je opravdu jednoduchá",
  "",
  "Konfigurace tiskáren je opravdu jednoduchá");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.1/thumbs/92.jpg",
  "images/3.1/fullsize/92.png",
  160, 120,
  "Vylepšený souborový dialog",
  "",
  "Vylepšený souborový dialog");

  $gallery->addImage(
  "images/3.1/thumbs/93.jpg",
  "images/3.1/fullsize/93.png",
  160, 120,
  "KOrganizer a KAdressbook",
  "",
  "KOrganizer a KAdressbook");

  $gallery->addImage(
  "images/3.1/thumbs/94.jpg",
  "images/3.1/fullsize/94.png",
  160, 120,
  "Lépe organizované Ovládací centrum",
  "",
  "Lépe organizované Ovládací centrum");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.1/thumbs/95.jpg",
  "images/3.1/fullsize/95.png",
  160, 120,
  "Sdílení pracovní plochy a monitor výkonu",
  "",
  "Sdílení pracovní plochy a monitor výkonu");

  $gallery->addImage(
  "images/3.1/thumbs/96.jpg",
  "images/3.1/fullsize/96.png",
  160, 120,
  "Přes 30 her včetně 4 nových",
  "",
  "Přes 30 her včetně 4 nových");

  $gallery->addImage(
  "images/3.1/thumbs/97.jpg",
  "images/3.1/fullsize/97.png",
  160, 120,
  "Správce verzí Cervisia",
  "",
  "Správce verzí Cervisia");

  $gallery->show();
?>

<?php
  include "footer.inc"
?>
