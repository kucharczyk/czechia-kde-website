<?php
  $page_title="Náhledy";
  include "header.inc";
?>

<p>Objevte ty nejlepší vlastnosti z nového K Desktop Environment.</p>

<p>Nepřehlédněte <a href="http://www.kde.org/announcements/visualguide-3.5.php">KDE 3.5: Grafický průvodce nových vlastností</a></p>

<center>

<?php
  $gallery = new ImageGallery("These are screenshots of the latest version of KDE.");
  $gallery->addImage("images/320x240/kde350.jpg", "kde350shots.php", "320", "240", "KDE 3.5.x Screenshots", "KDE 3.5.x Screenshots");
  $gallery->show();
?>

</center>

<p>
Celkový vzhled KDE se mezi jednotlivými verzemi mění. Proto jsou k dispozici náhledy jednotlivých verzí, které tyto změny zachycují.</p>

<p>Také můžete navštívit stránku <a href="http://wiki.kde.org/tiki-index.php?page=Screenshots">KDE Wiki Screenshots</a>, kde jsou další náhledy KDE.</p>

<center>
<?php
  $gallery = new ImageGallery("These are screenshots with links to pages that show more screenshots of how the different versions of KDE look like.");

  $gallery->addImage("images/160x120/kde340.jpg", "kde340shots.php", "160", "120", "Náhledy KDE 3.4", "Náhledy KDE 3.4");
  $gallery->addImage("images/160x120/kde330.jpg", "kde330shots.php", "160", "120", "Náhledy KDE 3.3", "Náhledy KDE 3.3");
  $gallery->startNewRow();

  $gallery->addImage("images/160x120/kde320.jpg", "kde320shots.php", "160", "120", "Náhledy KDE 3.2", "Náhledy KDE 3.2");
  $gallery->addImage("images/160x120/kde310.jpg", "kde310shots.php", "160", "120", "Náhledy KDE 3.1", "Náhledy KDE 3.1");
  $gallery->startNewRow();

  $gallery->addImage("images/160x120/kde300.jpg", "kde300shots.php", "160", "120", "Náhledy KDE 3.0", "Náhledy KDE 3.0");


  $gallery->startNewRow();

  $gallery->addImage("images/160x120/kde2x.jpg", "kde2shots.php", "160", "120", "Náhledy KDE 2", "Náhledy KDE 2");
  $gallery->addImage("images/160x120/kde1x.jpg", "kde1shots.php", "160", "120", "Náhledy KDE 1", "Náhledy KDE 1");


  $gallery->show();
?>

</center>

<?php
  include_once ("footer.inc");
?>
