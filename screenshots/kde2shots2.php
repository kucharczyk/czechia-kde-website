<?php
  $page_title = "KDE 2.0 Screenshots - Page 2";
  $site_root = "../";
  include_once ("header.inc");
?>

<table border="0" cellspacing="0" cellpadding="0" >
<tr>
<td colspan="2">
<p>
Here are some screenshots showing KDE 1.92, the third beta of KDE 2.0.
There are packages (source and binary)
<!-- <a href="ftp://ftp.us.kde.org/pub/kde/unstable/distribution/current/"> -->here <em>(link outdated)</em>,
and more recent source snapshots
<!-- <a href="ftp://ftp.us.kde.org/pub/kde/snapshots/current/"> -->here <em>(link outdated)</em>.
Download them and get your own impression!<br />
Click on a preview screenshot for a full sized version.
</p>
<p>
<a href="kde2shots.php">Previous page</a><br />
<a href="kde2shots3.php">Next page</a>
</p>
<hr />
</td>
</tr>

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
<a href="http://konqueror.kde.org/">Konqueror</a> is one of the killer
applications for KDE. Not only is it a great
file manager, it is also a high quality web browser. Konqueror supports all
major protocols including HTTP(S), FTP, SMB and the latest technologies such
as HTML4, CSS, Java and Javascript. The download window is the result of
simply dragging a link to the desktop and in case you were wondering... yes,
that _is_ a Flash animation. Konqueror easily runs all of your Netscape
plugins.
</p>
<ul>
<li>
view <a href="images/large/kde2b3_1.png">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2b3_1.png"><img src="images/mini/kde2b3_1.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
<a href="http://developer.kde.org/~kmail/index.html">KMail</a> has been
improved a lot for KDE2 and has become one of the easiest yet
most powerful graphical mail clients for *NIX. You can use Konqueror to
manage all your files, again with many improvements over the KDE1 file
manager. The run command dialog under Alt-F2 allows you to quickly run a
command when you don't feel like browsing through the menus. Note how the
icon automatically changes for each application! The windows decorations for
this snapshot have been set to the old KDE1 style. There are many styles
including BeOs and RISC OS mimicks as well as a special one for laptop
users.
</p>
<ul>
<li>
view <a href="images/large/kde2b3_2.png">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2b3_2.png"><img src="images/mini/kde2b3_2.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
The KDE Control Center allows you configure many settings ans preferences
under KDE2, including file associations, browsing preferences,
personalization and many more. Several modules can also run stand-alone,
such as the Configure Background dialog displayed on this screenshot.
</p>
<ul>
<li>
view <a href="images/large/kde2b3_3.png">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2b3_3.png"><img src="images/mini/kde2b3_3.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
If you cannot choose between the beauty of a GUI and the power of the
console, then you will love the optional transparency in Konsole allowing
you to get the best of both worlds. Along the same lines, make sure to try
the embedded konsole in Konqueror. This screenshot uses the System++ window
decorations with the Dark Blue color settings.
</p>
<ul>
<li>
view <a href="images/large/kde2b3_4.png">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2b3_4.png"><img src="images/mini/kde2b3_4.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
This is KDE 1.92 straight from the box. No settings have been changed at all.
KTip is a little program with many useful tips and clues for the beginning
KDE user.
</p>
<ul>
<li>
view <a href="images/large/kde2b3_5.png">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2b3_5.png"><img src="images/mini/kde2b3_5.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<p>
One of the most ambitious parts of KDE2 is the Koffice project, delivering a
complete Office suite. Shown is Kword, with more information about Koffice
and it's possibilities.
The <a href="http://www.koffice.org/">KOffice web site</a>
has more KOffice screenshots. BeOS users will recognize and love the window
decorations and colors on this screenshot.
</p>
<ul>
<li>
view <a href="images/large/kde2b3_6.png">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde2b3_6.png"><img src="images/mini/kde2b3_6.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<tr>
<td colspan="2">
<hr />
<a href="kde2shots.php">Previous page</a><br />
<a href="kde2shots3.php">Next page</a>
</td>
</tr>
</table>

<?php
  include_once ("footer.inc");
?>
