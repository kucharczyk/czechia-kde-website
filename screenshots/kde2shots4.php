<?php
  $page_title = "KDE 2.0 Screenshots - Japanese";
  $site_root = "../";
  include_once ("header.inc");
?>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2">
<p>
Here are some screenshots which show KDE using Japanese localisation. These screenshots are provided by the Japanese KDE Users Group (thanks to Satoshi Kinebuchi). M. Kinebuchi says: <i>"Although Junji Takagi (developer of the 
Japanese support in Qt-1.X) is an excellent X and 
i18n engineer, we owe a lot to the fact that Troll Tech acknowledged his work, 
and we are also allowed to distribute this localization work. We all appreciate
Troll Tech's kindly and openly decision. I think Troll Tech is a
<b>really open</b> enterprise."</i>
</p>
<p>
<a href="kde2shots3.php">Previous page</a>
</p>
<hr />
</td>
</tr>

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<ul>
<li>
view <a href="images/large/kde_jp_1.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde_jp_1.jpg"><img src="images/mini/kde_jp_1.jpg" alt="Japanese K Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<ul>
<li>
view <a href="images/large/kde_jp_2.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde_jp_2.jpg"><img src="images/mini/kde_jp_2.jpg" alt="Japanese K Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<!-- screenshot start -->
<tr style="vertical-align:top;">
<td style="padding-right: 2em;">
<ul>
<li>
view <a href="images/large/kde_jp_3.jpg">1024&times;768</a> version
</li>
</ul>
</td>
<td>
<a href="images/large/kde_jp_3.jpg"><img src="images/mini/kde_jp_3.jpg" alt="Desktop" border="0" height="240" width="320" /></a>
</td>
</tr>
<!-- screenshot end -->

<tr>
<td colspan="2">
<hr />
<a href="kde2shots3.php">Previous page</a>
</td>
</tr>
</table>

<?php
  include_once ("footer.inc");
?>
