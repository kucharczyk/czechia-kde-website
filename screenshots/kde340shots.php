<?php
  $page_title = "KDE 3.4 Screenshots";
  $site_root = "../";
  include "header.inc";
?>

<p> Here are some screenshots showing <a
href="../announcements/announce-3.4.php">KDE 3.4</a>, the current version
of the powerful K&nbsp;Desktop&nbsp;Environment, a free desktop environment for
Unix.</p>

<?php
  $gallery = new ImageGallery("These are screenshots of KDE 3.4.x");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot01.jpg",
  "images/3.4/snapshot01.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot02.jpg",
  "images/3.4/snapshot02.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot03.jpg",
  "images/3.4/snapshot03.png",
  160, 120,
  "",
  "",
  "");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.4/thumbs/snapshot04.jpg",
  "images/3.4/snapshot04.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot05.jpg",
  "images/3.4/snapshot05.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot06.jpg",
  "images/3.4/snapshot06.png",
  160, 120,
  "",
  "",
  "");

  $gallery->startNewRow();

    $gallery->addImage(
  "images/3.4/thumbs/snapshot07.jpg",
  "images/3.4/snapshot07.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot08.jpg",
  "images/3.4/snapshot08.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot09.jpg",
  "images/3.4/snapshot09.png",
  160, 120,
  "",
  "",
  "");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.4/thumbs/snapshot10.jpg",
  "images/3.4/snapshot10.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot11.jpg",
  "images/3.4/snapshot11.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot12.jpg",
  "images/3.4/snapshot12.png",
  160, 120,
  "",
  "",
  "");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.4/thumbs/snapshot13.jpg",
  "images/3.4/snapshot13.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot14.jpg",
  "images/3.4/snapshot14.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot15.jpg",
  "images/3.4/snapshot15.png",
  160, 120,
  "",
  "",
  "");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.4/thumbs/snapshot16.jpg",
  "images/3.4/snapshot16.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot17.jpg",
  "images/3.4/snapshot17.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot18.jpg",
  "images/3.4/snapshot18.png",
  160, 120,
  "",
  "",
  "");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.4/thumbs/snapshot19.jpg",
  "images/3.4/snapshot19.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot20.jpg",
  "images/3.4/snapshot20.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot21.jpg",
  "images/3.4/snapshot21.png",
  160, 120,
  "",
  "",
  "");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.4/thumbs/snapshot22.jpg",
  "images/3.4/snapshot22.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot23.jpg",
  "images/3.4/snapshot23.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot24.jpg",
  "images/3.4/snapshot24.png",
  160, 120,
  "",
  "",
  "");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/3.4/thumbs/snapshot25.jpg",
  "images/3.4/snapshot25.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot26.jpg",
  "images/3.4/snapshot26.png",
  160, 120,
  "",
  "",
  "");

  $gallery->addImage(
  "images/3.4/thumbs/snapshot27.jpg",
  "images/3.4/snapshot27.png",
  160, 120,
  "",
  "",
  "");

  $gallery->show();
?>

<?php
  include "footer.inc"
?>
