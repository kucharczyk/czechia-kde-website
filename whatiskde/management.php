<?php
  $site_onemenu = true;
  $site_root = "../";
  $site_title = "Správa projektu";
  include_once ("$site_root/media/includes/header.inc");
?>


<h1>Správa projektu</h1>
<p>
Asi 6 měsíců po založení KDE se začal projekt rozrůstat 
a připojilo se k němu velké množství lidí. Téměř až nekonečné
diskuze a hašteření začaly zpomalovat hlavní vývoj. V důsledku toho 
byly hlavní vývojáři nuceni otevřít nový komunikační kanál 
s omezeným právem posílání příspěvků. Do konference
<tt>kde-core-devel</tt> mají tedy možnost přispívat zpravidla jen 
hlavní vývojáři  - možnost číst konferenci mají samozřejmě 
všichni.
</p><p>
Členem vývojového týmu KDE, stejně jako u jiných Open Source projektů, se
může stát každý. Stejně tak ani jádro vývojového týmu není 
žádná uzavřená skupina. V současné době se skládá asi z 20
vývojářů a nejsou žádná pevně daná pravidla ohledně jejího členství.
Každý, kdo by se však chtěl stát jejím členem, musí mít zásluhy 
ve vývoji KDE a to po delší čas. Členové jádra vývojového týmu pak 
rozhodují v hlavních otázkách vývoje KDE, plánují vydání 
jednotlivých verzí. Stejně jako u ostatních projektů se tak neděje
pod taktovkou nějaké vůdčí osobnosti, ale o všech důležitých otázkách členové
demokraticky hlasují.
</p><p>
Vše ostatní se pak prodiskutovává v <a class="cs" href="../mailinglists/">
emailových konferencích</a> jako např. <tt>kde, kde-devel, kde-user, kde-cvs,</tt> a
dalších specializovaných na jednotlivé balíčky.
</p><p>
Nyní si můžete vybrat další kapitolu nebo se vrátit
<a class="cs" href="index.php">zpět</a>.
</p><ul>
<li><a class="cs" href="development.php">Vývoj KDE</a></li>
<li><a class="cs" href="join.php">Přidejte se ke KDE</a></li> 
</ul>

<?php include_once ("$site_root/media/includes/footer.inc"); ?>
