<?php
  $site_onemenu = true;
  $site_root = "../";
  $site_title = "Model vývoje KDE";
  include_once ("$site_root/media/includes/header.inc");
?>

<h1 align="center">Model vývoje KDE</h1>
<p>
Projekt velikosti KDE může být úspěšný pouze v případě, že dokáže
přilákat velké množství vývojářů ochotných věnovat
svůj drahocenný volný čas bez sebemenší naděje na nějaké 
finanční ohodnocení. Z tohoto důvodu nesmí vývoj nudit - stále 
zde musí být něco, s čím si může vývojář hrát a experimentovat.
Toto je hlavní věc, kterou jsme se naučili z chyb
softwarových projektů jako např. "GNUStep". 
</p><p>
Podle našeho názoru není nejlepší cestou vymyslet projekt
na papíře, jedno jak brilantní by tento návrh byl, a pak
se snažit přesvědčit vývojáře, aby  tyto návrhy uskutečňovali
bez možnosti prostoru na realizaci jejich vlastních myšlenek.
Domníváme se, že je bezpodmínečně nutné, aby v každé fázi vývoje existovala 
jeho funkční implementace, která může být ukazována a demonstrována,
byť zatím nedokonalá a nekompletní, a postupně zdokonalována, i když to 
občas znamená zahodit některé části už nepotřebného kódu, nebo
kompletně přepsat, ukáže-li se, že jejich návrh byl neuspokojující.
Ač se zdá tento přístup k vývoji značně neefektivní,
celkový výsledek je víceméně uspokojivý, jelikož
velké množství vývojářů produkuje velké množství
částečně neefektivní práce.
</p><p>
Je tedy lepší mít 100 nadšených
a dostatečně motivovaných vývojářů, kteří 
interaktivně zlepšují projekt, než 5 pracujících 
podle předem daného návrhu. Tato pětice by jistě za chvíli
byla znuděná kvůli minimálnímu prostoru pro jejich vlastní 
nápady a postupně by vývoj vzdala.
</p><p>
Jak to funguje v praxi ukazuje schematicky následující obrázek.
</p>
<div align="center">
<img src="images/devel2.png" height="123" width="450" alt="Začarovaný kruh vývoje" />
</div>
<p>
KDE je psáno v programovacím jazyce C++ a celý projekt KDE je
objektově orientován. Věříme, že efektnost a znovupoužitelnost
jednotlivých komponent, díky jejich objektovému návrhu, je základním
klíčem k úspěchu. KDE poskytuje vývojový rámec, podobný
MFC/Com/ActiveX od fy. Microsoft, umožňující rychlý vývoj aplikací
a zařazení už existujících komponent a technologií.
</p><p>
Všechny aplikace KDE vycházejí z objektu <tt>KApplication</tt> a odvozují se
z objektu <tt>KMainWindow</tt>. Aplikace vytvořené touto cestou se pak automaticky 
přizpůsobují standardům KDE.
</p><p>
Základní schéma KDE pro vývojáře je nastíněno v následujícím obrázku.
</p>

<div align="center">
<img src="images/devel1.png" height="300" width="400" alt="Schéma KDE" /> 
</div>
<p>
Nyní si můžete vybrat další kapitolu nebo se vrátit
<a class="cs" href="index.php">zpět</a>.
</p><ul>
<li><a class="cs" href="join.php">Přidejte se ke KDE</a></li> 
</ul>

<?php include_once ("$site_root/media/includes/footer.inc"); ?>
