<?php
$this->appendLink("Projekt KDE","project.php");
$this->appendLink("Projekt KOffice","koffice.php");
$this->appendLink("Projekt KDevelop","kdevelop.php");
$this->appendLink("Správa projektu","management.php");
$this->appendLink("Model vývoje","devmodel.php");
$this->appendLink("Pohled vývojáře","devview.php");
$this->appendLink("Lokalizace","i18n.php");
$this->appendLink("Manifest","manifest.php");
$this->appendLink("KDE Free Qt Foundation","kdeqtfoundation.php");
$this->appendLink("Otázky kolem Qt","qtissue.php");
?>
