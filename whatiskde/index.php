<?php
  $site_onemenu = true;
  $site_root = "../";
  $site_title = "Co je to KDE?";
  include_once ("$site_root/media/includes/header.inc");
?>

<h1>Co je to KDE?</h1>
<p>
Následující stránky by Vám měli poskytnout základní
informace o KDE a usnadnit Vám orientaci, zvláště setkáváte-li
se s KDE poprvé. Věříme, že se nám podařilo pokrýt
širokou oblast otázek kolem KDE a doufáme, že Vám tento
přehled pomůže v porozumění struktury KDE. 
</p>

<h2>Obecný popis KDE</h2>
<p>
KDE je moderní prostředí pracovní plochy pro počítače s operačním systémem
typu UNIX. KDE by rádo naplnilo potřebu jednoduchého používání pracovní
stanice s Unixem, podobně jako pracovní prostředí operačního systému MacOS
nebo Win 9x/NT. Věříme, že operační systémy typu UNIX jsou nejlepší
operační systémy, které byly dosud vyvinuty. Známým faktem je, že UNIX
převládá na poli serverů a je upřednostňovanou platformou pro počítačové
profesionály a vědce - bez UNIXu by nikdy nebyl Internet jaký je. Navzdory
tomu UNIX nesplňuje požadavky průměrného uživatele a díky nedostatku
jednoduchého ovládání si ve většině případů nenašel cestu k typickému
uživateli počítače v kanceláři či doma. Tento fakt je obzvlášť nešťastný,
neboť řada implementací UNIXu (<a class="cs"
href="http://www.linux.cz">Linux</a>, <a class="en"
href="http://www.freebsd.cz">FreeBSD</a>, <a class="en"
href="http://www.netbsd.org">NetBSD</a>, atd.) je dostupná zdarma na
Internetu a všechny jsou většinou mimořádně kvalitní a stabilní.
</p>

<h2>KDE - Prostředí pracovní plochy</h2>
<p>
Nyní KDE přináší možnost použití moderního prostředí pracovní plochy i pod
operačními systémy typu UNIX. Dohromady s některými volně dostupnými
implementacemi UNIXu jako je Linux vytváří kompletní, volně dostupnou a
otevřenou platformu pro kohokoliv, spolu se zdrojovými kódy s možností
jejich modifikace. Jakkoliv tu bude vždy prostor pro vylepšování, věříme,
že se nám podařilo vytvořit životaschopnou alternativu k některým dnes
značně rozšířeným komerčním operačním systémům. Doufáme, že kombinace
UNIX/KDE přinese konečně průměrnému uživateli možnost spolehlivé,
stabilní a monopoly neovládané práce s počítačem, stejně jako
si počítačoví profesionálové a vědci po celém světě užívají léta.
</p>

<h2>KParts - Rámec pro vývoj aplikací</h2>
<p> 
Vytvářet aplikace přímo pro UNIX/X11 je většinou velice obtížný a namáhavý
proces. Tým KDE si uvědomuje fakt, že úroveň počítačové platformy
zpravidla odpovídá počtu kvalitních aplikací používaných uživateli na dané
platformě. Z tohoto důvodu tým KDE vyvinul dokumentovaný rámec pro vývoj
aplikací, zavádějící poslední postupy v technologiích aplikačních rámců, a
zařadil se tak přímo do souboje mezi takové oblíbené vývojové rámce, jako
jsou například technologie firmy Microsoft MFC/COM/ActiveX.  
Dokumentovaná technologie KParts dovoluje vývojářům snadno a rychle
vytvářet prvotřídní aplikace dosahující technologické špičky. 
</p> 

<h2>KDE - Balík aplikací</h2>
<p>
Díky KParts vzniklo (a stále vzniká) poměrně velké množství 
<a class="en" href="http://www.apps.com">aplikací</a> pro KDE. Část z nich
je už obsažena v základní distribuci, zbylé jsou zpravidla ke stažení na
internetu. Mezi nejvýznamnější patří balík kancelářských aplikací
<a class="en" href="http://www.koffice.org">KOffice</a> a
univerzální prohlížeč 
<a class="en" href="http://www.konqueror.org">Konqueror</a>.
Dále můžeme zmínit programy na přehrávání 
<a class="en" href="http://multimedia.kde.org/">multimediálních</a> 
souborů,
<a class="en" href="http://games.kde.org/">hry</a>, a nově i 
<a class="en" href="http://edu.kde.org">výukové programy</a>,
či balík aplikací typu
<a class="en" href="http://pim.kde.org/">osobní manažer</a>.
</p>

<h2>Internacionalizace KDE</h2>
<p>
Tým KDE si plně uvědomuje, že většina uživatelů
by měla ráda dostupné aplikace a dokumentaci ve svém
mateřském jazyce. Proto 
<a class="en" href="http://i18n.kde.org/teams/">týmy</a> 
sdružené na <a class="en" href="http://i18n.kde.org/">i18n.kde.org</a> 
(i18n je akronym pro slovo anglické slovo internationalization)
pracují na překladu jednotlivých aplikací a dokumentace do 
řady cizích jazyků. Současně s tím se řada týmů stará o webovské 
<a class="en" href="http://www.kde.org/international/">stránky</a> ve svém národním
jazyce. Pokud vás problematika překladu zaujala, navštivte
stránku věnovanou
<a class="cs" href="/helping/index.php#trans">spolupráci</a>
při vývoji KDE, nebo přímo stránky českého překladatelského týmu na   
<a class="cs" href="http://kde-czech.sourceforge.net/">kde-czech.sourceforge.net</a>.
</p>

<p>
Nyní bychom Vás rádi pozvali k dalšímu poznávání KDE:
</p><ul>
<li><a class="cs" href="project.php">Projekt KDE</a></li>
<li><a class="cs" href="management.php">Správa projektu</a></li>
<li><a class="cs" href="development.php">Vývoj KDE</a></li>
<li><a class="cs" href="join.php">Přidejte se ke KDE</a></li> 
</ul><p>
Na další obecné otázky kolem KDE naleznete odpovědi v sekci 
<a class="cs" href="/info/">přehled vlastností KDE</a>
nebo v sekci 
<a class="en" href="http://www.kde.org/documentation/faq/index.html">KDE
FAQ</a>. Můžete si také prohlédnout některou z 
<a class="en" href="/presentation/">prezentací KDE</a> nebo se podívat na 
<a class="cs" href="/screenshots/">Náhledy na KDE</a>.
</p>

<?php include_once ("$site_root/media/includes/footer.inc"); ?>
