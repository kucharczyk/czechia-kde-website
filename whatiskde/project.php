<?php
  $site_onemenu = true;
  $site_root = "../";
  $site_title = "Projekt KDE";
  include_once ("$site_root/media/includes/header.inc");
?>

<h1 align="center">Projekt KDE</h1> 
<p>
Tým KDE je otevřená skupina sestávající se z několika stovek vývojářů 
z celého světa spojených myšlenkou vývoje kvalitního otevřeného
softwaru. Projekt KDE je otevřený softwarový projekt.
Veškerý zdrojový kód KDE je k dispozici pod licencemi LGPL/GPL. To znamená,
že ho kdykoliv kdokoliv může modifikovat nebo dále rozšiřovat.
To v důsledku také znamená, že KDE je a vždy bude zdarma pro kohokoliv.
</p>
<h2>Důvody vzniku KDE</h2>
<p>
Tradiční Unixové prostředí X11 má mezi jinými i následující nedostatky:
</p>
<ul>
<li>Neexistence protokolu "Drag and Drop"</li>
<li>Nemožnost jednoduché konfigurace</li>
<li>Nespecifikovaný systém nápovědy</li>
<li>Nejednotný rámec pro vývoj aplikací a dokumentace</li>
<li>Nedostatek síťové transparentnosti pro aplikace</li>
<li>Naučit se pracovat s X11 je pracné a únavné</li>
</ul>

<h2>Cíle projektu KDE</h2>
<p>
Odstranit výše zmíněné problémy, tj. přinést uživateli:
</p>
<ul>
<li>Moderní a vzhledově kvalitní prostředí pracovní plochy</li>
<li>Integrovaný systém nápovědy umožňující standardním způsobem přístup k nápovědě prostředí KDE a jeho aplikací</li>
<li>Konzistentní vhled všech aplikací KDE, tj. standardizovaná menu, panely nástrojů, schémata barev, atd.</li>
<li>Dostupnost prostředí KDE a jeho aplikací v jednotlivých národních jazycích</li>
<li>Možnost centralizované konfigurace celého prostředí</li>
<li>Co největší počet aplikací</li>
</ul>
<a name="history" id="history"></a>
<h2>Trocha historie</h2>
<p></p>
<table align="center" cellspacing="0"  cellpadding="0"  border="0" width="90%">
<tr>
  <td>listopad 1996</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Založen projekt KDE.</td>
</tr><tr>
  <td>15. srpna 1997</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>První setkání vývojářů - <a class="en" href="http://www.kde.org/kde-one.html">KDE-ONE</a>.</td>
</tr><tr>
  <td>20. října 1997</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/beta1announce.php">Beta 1</a>.</td>
</tr><tr>
  <td>27. listopadu 1997</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/beta2announce.php">Beta 2</a>.</td>
</tr><tr>
  <td>prosinec 1997</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Byla založeno KDE <a class="en" href="http://www.kde.org/areas/kde-ev/">e.V.i.G.</a></td>
</tr><tr>
  <td>1. února 1998</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/beta3announce.php">Beta 3</a>.</td>
</tr><tr>
  <td>8. března 1998</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Oznámen vznik KDE Free Qt Foundation.</td>
</tr><tr>
  <td>19. dubna 1998</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/beta3announce.php">Beta 4</a>.</td>
</tr><tr>
  <td>12. července 1998</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-1.0.php">1.0</a>.</td>
</tr><tr>
  <td>6. února 1999</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze 1.1.</td>
</tr><tr>
  <td>5. května 1999</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-BW-1.1.1.php">1.1.1</a>.</td>
</tr><tr>
  <td>7.-10. října 1999</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Druhé setkání vývojářů - <a class="en" href="http://www.kde.org/kde-two.html">KDE-TWO</a>.</td>
</tr><tr>
  <td>15.prosince 1999</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-1.89.php">1.89</a>.</td>
</tr><tr>
  <td>11. května 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-1.90.php">1.90 (KDE 2 beta)</a>.</td>
</tr><tr>
  <td>14.června 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-1.91.php">1.91 (KDE 2 beta2).</a></td>
</tr><tr>
  <td>9.-19. července 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Třetí setkání vývojářů - <a class="en" href="http://www.kde.org/announcements/k3b-announce.php">KDE-THREE</a>.</td>
</tr><tr>
  <td>25. července 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-1.92.php">1.92 (KDE2  beta3)</a>.</td>
</tr><tr>
  <td>23. srpna 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-1.93.php">1.93 (KDE2 beta4)</a>.</td>
</tr><tr>
  <td>4. září 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Firma Trolltech <a class="en" href="http://www.trolltech.com/company/announce/gpl.php">uvolnila Qt</a> pod licencí GPL.</td>
</tr><tr>
  <td>15. září 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-1.94.php">1.94 (KDE 2  beta5)</a>.</td>
</tr><tr>
  <td>10. října 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-2.0-RC2.php">RC-2.0</a>.</td>
</tr><tr>
  <td>23. října 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-2.0.php">2.0</a>.</td>
</tr><tr>
  <td>5. prosince 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-2.0.1.php">2.0.1</a>.</td>
</tr><tr>
  <td>16. prosince 2000</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-2.1-beta1.php">2.1 Beta 1</a>.</td>
</tr><tr>
  <td>31.ledna 2001</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-2.1-beta2.php">2.1 Beta 2</a>.</td>
</tr><tr>
  <td>26. února 2001</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="cs" href="/announcements/announce-2.1.php">2.1</a>.</td>
</tr><tr>
  <td>27. března 2001</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-2.1.1.php">2.1.1</a>.</td>
</tr><tr>
  <td>30. dubna 2001</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání knihoven KDE ve verzi <a class="en" href="http://www.kde.org/announcements/announce-2.1.2.php">2.1.2</a>.</td>
</tr><tr>
  <td>4. července 2001</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-2.2-beta1.php">2.2 Beta1</a>.</td>
</tr><tr>
  <td>15. srpna 2001</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="cs" href="/announcements/announce-2.2.php">2.2</a>.</td>
</tr><tr>
  <td>19. září 2001</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-2.2.1.php">2.2.1</a>.</td>
</tr><tr>
  <td>2001</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-3.0alpha1.php">3.0 Alfa1</a>.</td>
</tr><tr>
  <td>3. dubna 2002</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-3.0.php">3.0</a>.</td>
</tr>
<tr>
  <td>28. ledna 2003</td><td>&nbsp;-&nbsp;&nbsp;</td>
  <td>Vydání verze <a class="en" href="http://www.kde.org/announcements/announce-3.1.php">3.1</a>.</td>
</tr>
</table>

<p>
Nyní si můžete vybrat další kapitolu nebo se vrátit
<a class="cs" href="index.php">zpět</a>.
</p><ul>
<li><a class="cs" href="management.php">Správa projektu</a></li>
<li><a class="cs" href="development.php">Vývoj KDE</a></li>
<li><a class="cs" href="join.php">Přidejte se ke KDE</a></li> 
</ul>

<?php include_once ("$site_root/media/includes/footer.inc"); ?>
