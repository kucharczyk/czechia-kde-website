<?php

$this->setName ("Domovská stránka");

$section =& $this->appendSection("Poznejte KDE");
$section->appendDir("Co je KDE?","whatiskde");
$section->appendDir("Náhledy","screenshots");
$section->appendDir("Zkuste KDE","trykde");
$section->appendDir("Tiskové zprávy","announcements");
$section->appendDir("Obecné informace","info");
$section->appendDir("Dokumentace","documentation");
$section->appendDir("Podpora KDE","support");
$section->appendDir("Reklama","stuff");
$section->appendDir("Historie","history");

$section =& $this->appendSection("Stažení");
$section->appendDir("Stabilní verze", "download");
$section->appendLink("Zdrojový kód", "http://techbase.kde.org/Getting_Started_%28cs%29", false);
$section->appendDir("FTP zrcadla", "mirrors");

$section =& $this->appendSection("Komunita");
$section->appendDir("Lidé","people");
$section->appendDir("Emailové konference","mailinglists");
$section->appendLink("Fórum","http://forum.kde.org/", false);

$section =& $this->appendSection("Vývoj");
$section->appendDir("Zapojte se","getinvolved");
$section->appendLink("Pro vývojáře","http://techbase.kde.org/Getting_Started_%28cs%29", false);
$section->appendLink("Databáze chyb","http://bugs.kde.org/", false);
$section->appendLink("Pro překladatele","http://i10n.kde.org/", false);
$section->appendLink("Zdrojový kód","http://lxr.kde.org/", false);
$section->appendLink("WebSVN","http://websvn.kde.org/", false);

$section =& $this->appendSection("Různé");
$section->appendDir("Rodina serverů KDE","family");
$section->appendLink("KDE v jiných jazycích","family/international.php");
?>
